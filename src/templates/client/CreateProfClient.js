import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  showModal, // var abrir modal
  msgModal, // var mensaje del modal
  hideModal,
} from '../../ducks/createClient';
// import { useDispatch, useSelector } from "react-redux";
import ClientForm from './ClientForm';
// import Modal from '../../components/Modal';
import ModalActions from '../../components/ModalActions';

// import { addText , saved, cutText} from '../../ducks/access';

// import { saved } from '../../../ducks/access';

const CreateProfClient = () => {
  const dispatch = useDispatch();
  const showModalTemp = useSelector(state => showModal(state));
  const msgModalTemp = useSelector(state => msgModal(state));

  const setShow = () => {
    dispatch(hideModal());
  };

  return (
    <div>
      <h1>
        COMO administrador QUIERO crear un perfil de cliente PARA registrarlo en
        Arawato.{' '}
      </h1>
      <ClientForm action="create" />
      {/* clientList // MOSTRAR LISTA DE CLIENTES EN LA MISMA PANTALLA AL PRESIONAR ENVIAR
        ? clientList.map((client, i) => (
            <div key={i}>
              <div>
                <h1>Cliente #{i + 1} - </h1> {client.name} {client.lastName}
              </div>

              <div>Email: {client.email}</div>
              <div>Empresa: {client.company}</div>
              <div>Telefono celular: {client.phone}</div>
              <div>Cargo: {client.position}</div>
              <div>Estatus: {client.status}</div>
            </div>
          ))
        : null */}

      <ModalActions
        showModalTemp={showModalTemp}
        onHide={() => setShow()}
        msgModalTemp={msgModalTemp}
        actionType="CREATE_CLIENT"
      />

      {/* <Modal show={showModalTemp} onHide={() => setShow()}>
        <Background>
          <div
            style={{
              backgroundColor: '#ffffff99',
              height: '100%',
              paddingTop: '30px',
            }}
          >
            <h1>MODAL DE CREATE CLIENT</h1>
          </div>
          <div className="total-center flex-column">
            <div>
              {msgModalTemp}
              {/* <Description>
                  https://api.whatsapp.com/send?phone=584142528976
                </Description> 
            </div>
          </div>
        </Background>
      </Modal> */}
    </div>
  );
};

/* const Background = styled.div`
  background-image: url('/assets/images/bg-contact.jpg');
  width: 100%;
  height: 200px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`; */

export default CreateProfClient;
