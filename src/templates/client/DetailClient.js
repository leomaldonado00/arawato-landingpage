import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Loading from '../../Loading/Loading';
import {
  isSearchLoading, // var
  detailClient, // var
  editForm, // var muestra o no el formulario de editar cliente /form o detalle
  // comments,
  detailClientById,
  showForm, // action que cambia la var editForm para muestra el formulario
  error, // var
  showModal, // var abrir modal
  msgModal, // var mensaje del modal
  hideModal,
} from '../../ducks/createClient';
import ModalActions from '../../components/ModalActions';
import ClientForm from './ClientForm';

// import { dispatch } from "../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/rxjs/internal/observable/pairs";

const DetailClient = ({ match, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const detailClientTemp = useSelector(state => detailClient(state));
  const errorTemp = useSelector(state => error(state));
  const editFormTemp = useSelector(state => editForm(state));
  const showModalTemp = useSelector(state => showModal(state));
  const msgModalTemp = useSelector(state => msgModal(state));

  const setShow = () => {
    dispatch(hideModal());
  };

  const [edit, setEdit] = useState(false);
  // const commentsTemp = useSelector(state => comments(state));

  useEffect(() => {
    // console.log("useEffect_DetailClient");
    console.log('match_DetailClient', match);
    const idClientDetail = match.params.id;
    dispatch(detailClientById({ idClientDetail }));
  }, [dispatch, match]);

  /* const seeComments0 = id => {
    // dispatch(seeComments({ id }));
    history.push(`/comments?ClientId=${id}`);
  }; */
  const editFunc = siono => {
    if (siono === 'show') {
      console.log('show');
      return true;
    }
    if (editFormTemp) {
      return true;
    }
    return false;
  };
  const showForm0 = (idClient, show) => {
    dispatch(showForm({ idClient, show }));
  };
  console.log('editFormTemp', editFormTemp);

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {detailClientTemp ? (
        <div>
          {/* <button
            onClick={() =>
              history.push("/pHold", {
                ancla: ">>CONTENIDO DEL ESTADO DE HISTORY LOCATION<<"
              })
            }
          >
            {" "}
            Phold (enviando STATE de history.location){" "}
        </button> */}
          {/* <button onClick={() => history.goBack()}>BACK </button> */}

          {editFormTemp ? (
            <ClientForm action="edit" idClientDetail={detailClientTemp.id} />
          ) : (
            <div>
              {
                <div>
                  {' '}
                  {detailClientTemp && detailClientTemp.userId} .{' '}
                  {detailClientTemp && detailClientTemp.id} -{' '}
                  {detailClientTemp && detailClientTemp.title} :{' '}
                </div>
              }
              {<div> {detailClientTemp && detailClientTemp.body} </div>}
              <button
                type="button"
                onClick={() => showForm0(detailClientTemp.id, true)}
              >
                EDITAR cliente
              </button>
            </div>
          )}

          {/* <button
            onClick={() => {
              seeComments0(detailClientTemp.id);
            }}
          >
            VER COMMENTS
        </button> */}
          {/* commentsTemp.map((comm, i) => (
                <div key={comm.id}>
                <h1>
                    {comm.ClientId} - {comm.name}
                </h1>
                <div>{comm.email}</div>
                <p>{comm.bady}</p>
                </div>
            )) */}
          <ModalActions
            showModalTemp={showModalTemp}
            onHide={() => setShow()}
            msgModalTemp={msgModalTemp}
            actionType="EDIT_CLIENT(Detalle de Cliente)"
          />
        </div>
      ) : null}

      {errorTemp ? (
        <div>
          <h1>{errorTemp}</h1>
          <ModalActions
            showModalTemp={showModalTemp}
            onHide={() => setShow()}
            msgModalTemp={msgModalTemp}
            actionType="DETAIL_CLIENT_BY_ID(Detalle de Cliente)"
          />
        </div>
      ) : null}
    </div>
  );
};

export default DetailClient;
