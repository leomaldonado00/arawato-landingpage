import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../../Loading/Loading';
import {
  isSearchLoading, // var
  detailClient, // var
  // comments,
  detailClientById,
  error, // var
} from '../../ducks/createClient';
import ClientForm from './ClientForm';

// import { useDispatch, useSelector } from "react-redux";
// import { addText , saved, cutText} from '../../ducks/access';

// import { saved } from '../../../ducks/access';

const EditProfClient = ({ match, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const detailClientTemp = useSelector(state => detailClient(state));
  const errorTemp = useSelector(state => error(state));

  useEffect(() => {
    // console.log("useEffect_DetailClient");
    console.log('match_EditClient', match);
    const idClientDetail = match.params.id;
    dispatch(detailClientById({ idClientDetail }));
  }, [dispatch, match]);

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {detailClientTemp ? (
        <div>
          {' '}
          <h1>
            COMO administrador QUIERO Editar un perfil de cliente PARA cambiar
            datos de la ficha del cliente.{' '}
          </h1>
          <h2>Aqui se edita el perfil del cliente # {detailClientTemp.id}</h2>
          <br />
          <ClientForm action="edit" />{' '}
        </div>
      ) : null}
      {/* clientList // MOSTRAR LISTA DE CLIENTES EN LA MISMA PANTALLA AL PRESIONAR ENVIAR
        ? clientList.map((client, i) => (
            <div key={i}>
              <div>
                <h1>Cliente #{i + 1} - </h1> {client.name} {client.lastName}
              </div>

              <div>Email: {client.email}</div>
              <div>Empresa: {client.company}</div>
              <div>Telefono celular: {client.phone}</div>
              <div>Cargo: {client.position}</div>
              <div>Estatus: {client.status}</div>
            </div>
          ))
        : null */}
      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default EditProfClient;
