import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  editClient,
  createClient,
  showForm, // action que cambia la var editForm para muestra/oculta el formulario
} from '../../ducks/createClient'; // por ahora solo en ACTIONS: debe hacer llamada para editar client y luego "put" la accion de mostrar el cliente editado (edit..)
// import { addText , saved, cutText} from '../../ducks/access';

// import { saved } from '../../../ducks/access';

const ClientForm = ({ action, idClientDetail }) => {
  const dispatch = useDispatch();

  console.log(action);
  const [clientList, setClientList] = useState([]);
  const [formLogin, setFormLogin] = useState({
    name: `${action === 'create' ? '' : 'name Actual llamado de API'}`,
    lastName: `${action === 'create' ? '' : 'lastName Actual llamado de API'}`,
    email: `${action === 'create' ? '' : 'email Actual llamado de API'}`,
    company: `${action === 'create' ? '' : 'company Actual llamado de API'}`,
    phone: `${action === 'create' ? '' : 'phone Actual llamado de API'}`,
    position: `${action === 'create' ? '' : 'position Actual llamado de API'}`,
    status: `${
      action === 'create' ? 'creado' : 'status Actual llamado de API'
    }`, // Status inicial por defecto!/o el actual?

    password: '',
    date: '',
    hour: '',
    ip: '',
    url: '',
    pizza: false,
    pasta: false,
    arroz: false,
    ciudad: '',

    Ename: '',
    ElastName: '',
    Eemail: '',
    Ecompany: '',
    Ephone: '',
    Eposition: '',
    Estatus: '',

    Epassword: '',
    Edate: '',
    Ehour: '',
    Eip: '',
    Eurl: '',
    Ecomidas: '',
    Eciudad: '',

    Eflag: false,
  });
  const [editComplete, setEditComplete] = useState(false);

  const validate = dataLogin => {
    let errores = {};
    if (
      !dataLogin.name ||
      !dataLogin.lastName ||
      !dataLogin.email ||
      !dataLogin.company ||
      !dataLogin.phone ||
      !dataLogin.position ||
      !dataLogin.status
      /* !dataLogin.password ||
      !dataLogin.date ||
      !dataLogin.hour ||
      !dataLogin.ip ||
      !dataLogin.url ||
      !dataLogin.ciudad ||
      (!dataLogin.arroz && !dataLogin.pasta && !dataLogin.pizza) */
    ) {
      if (dataLogin.name.length === 0) {
        errores = {
          ...errores,
          Ename: 'El nombre es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.lastName.length === 0) {
        errores = {
          ...errores,
          ElastName: 'El apellido es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.email.length === 0) {
        errores = { ...errores, Eemail: 'El email es requerido', Eflag: true };
      }
      if (dataLogin.company.length === 0) {
        errores = {
          ...errores,
          Ecompany: 'La Empresa es requerida',
          Eflag: true,
        };
      }
      if (dataLogin.phone.length === 0) {
        errores = {
          ...errores,
          Ephone: 'El teléfono celular es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.position.length === 0) {
        errores = {
          ...errores,
          Eposition: 'El cargo es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.status.length === 0) {
        errores = {
          ...errores,
          Estatus: 'El Estatus es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.password.length === 0) {
        errores = {
          ...errores,
          Epassword: 'El password es requerido',
          Eflag: true,
        };
      }
      if (dataLogin.date.length === 0) {
        errores = { ...errores, Edate: 'El date es requerido', Eflag: true };
      }
      if (dataLogin.hour.length === 0) {
        errores = { ...errores, Ehour: 'El hour es requerido', Eflag: true };
      }
      if (dataLogin.ip.length === 0) {
        errores = { ...errores, Eip: 'El ip es requerido', Eflag: true };
      }
      if (dataLogin.url.length === 0) {
        errores = { ...errores, Eurl: 'El url es requerido', Eflag: true };
      }

      if (dataLogin.ciudad.length === 0) {
        errores = {
          ...errores,
          Eciudad: 'Es requerida una ciudad de residencia!!!',
          Eflag: true,
        };
      }
      if (!dataLogin.arroz && !dataLogin.pasta && !dataLogin.pizza) {
        errores = {
          ...errores,
          Ecomidas: 'Es requerida al menos 1 comida!!!',
        };
      }
      console.log('errores:', errores);
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    } /* else {
      setdataLogin({ ...dataLogin, Eflag: false });
    } */
    if (
      dataLogin.name.length !== 0 &&
      !/^[a-zA-z\s]{3,50}$/.test(dataLogin.name)
    ) {
      errores = {
        ...errores,
        Ename: 'El nombre debe tener minimo 3 caracteres, no especiales.',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.lastName.length !== 0 &&
      !/^[a-zA-z\s]{3,50}$/.test(dataLogin.lastName)
    ) {
      errores = {
        ...errores,
        ElastName: 'El apellido debe tener minimo 3 caracteres, no especiales.',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.email.length !== 60 &&
      !/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(
        dataLogin.email,
      )
    ) {
      errores = {
        ...errores,
        Eemail: 'Asegúrese de que el email sea corecto',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.company.length !== 0 &&
      !/^(.){3,50}$/.test(dataLogin.company)
    ) {
      errores = {
        ...errores,
        Ecompany: 'El nombre de empresa debe contener de 3 a 50 caracteres',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.phone.length !== 0 &&
      !/^[\d\+]{1,17}$/.test(dataLogin.phone)
    ) {
      errores = {
        ...errores,
        Ephone: 'Verifique que el numero telefónico sea correcto',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.position.length !== 0 &&
      !/^(.){3,40}$/.test(dataLogin.position)
    ) {
      errores = {
        ...errores,
        Eposition: 'El nombre del cargo debe contener de 3 a 50 caracteres',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.password.length !== 0 &&
      !/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/.test(dataLogin.password)
    ) {
      errores = {
        ...errores,
        Epassword:
          'Contraseña debe tener al menos un numero y una mayuscula, entre 8 y 16 caracteres(sin simbols)',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.date.length !== 0 &&
      !/^(?:3[01]|[12][0-9]|0?[1-9])([\-/.])(0?[1-9]|1[1-2])\1\d{4}$/.test(
        dataLogin.date,
      )
    ) {
      errores = {
        ...errores,
        Edate: 'fecha debe ser dataato dd/mm/aaaa.',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.hour.length !== 0 &&
      !/^(?:0?[1-9]|1[0-2]):[0-5][0-9]\s?(?:[aApP](\.?)[mM]\1)?$/.test(
        dataLogin.hour,
      )
    ) {
      errores = {
        ...errores,
        Ehour: 'Hora debe ser dataato hh:mmXM',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.ip.length !== 0 &&
      !/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
        dataLogin.ip,
      )
    ) {
      errores = {
        ...errores,
        Eip: 'La dirección ip es incorrecta.',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.url.length !== 0 &&
      !/^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$/.test(dataLogin.url)
    ) {
      errores = {
        ...errores,
        Eurl: 'La dirección de url es incorrecta.',
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    setFormLogin({ ...dataLogin, Eflag: false });
    return true;

    // }
  };

  const escrib = e => {
    // console.log(e.target.name, e.target.value, e.target.checked, e.target.type);
    if (e.target.type === 'checkbox' /* || e.target.type === "radio" */) {
      setFormLogin({
        ...formLogin,
        [e.target.name]: e.target.checked,
        [`E${e.target.value}`]: '',
      });
    }
    setFormLogin({
      ...formLogin,
      [e.target.name]: e.target.value,
      [`E${e.target.name}`]: '',
    });

    /* if (formLogin.email !== 0) {
          setFormLogin({ ...formLogin, Eemail: "" });
        } */
    console.log(formLogin);
  };
  const showState = () => {
    console.log('STATE', formLogin);
  };
  const validate0 = dataLogin => {
    const {
      name,
      lastName,
      email,
      company,
      phone,
      position,
      status,
    } = dataLogin;
    const client = { name, lastName, email, company, phone, position, status };
    const siono = validate(dataLogin);
    if (siono) {
      if (action === 'create') {
        console.log('CREAR: id: ', idClientDetail);
        dispatch(createClient({ dataLogin })); // OjO: LLAMADA PARA CREAR CLIENTE (POST)...luego "put_action_saga" para el mensaje de success o error en el MODAL..!

        setFormLogin({
          ...formLogin,
          name: '',
          lastName: '',
          email: '',
          company: '',
          phone: '',
          position: '',
        });
        setClientList([...clientList, client]);
      }
      if (action === 'edit') {
        console.log('EDITAR: id: ', idClientDetail);

        dispatch(editClient({ idClientDetail })); // OjO: LLAMADA PARA EDITAR CLIEMTE (PUT o PATCH)...luego "put" una var de estado para mostrar el perfil editado en la misma pantalla!
      }
    } else {
      if (action === 'create') {
        console.log('NO CREAR');
      }
      if (action === 'edit') {
        console.log('NO EDITAR');
      }
    }
    console.log(formLogin);
  };

  const showForm0 = (idClient, show) => {
    dispatch(showForm({ idClient, show }));
  };

  return (
    <div>
      {formLogin.Ename.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.Ename}</p>
      ) : null}
      <input
        type="text"
        name="name"
        placeholder="Nombre"
        onChange={e => escrib(e)}
        value={formLogin.name}
        maxLength="50"
      />
      {formLogin.ElastName.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.ElastName}</p>
      ) : null}
      <input
        type="text"
        name="lastName"
        placeholder="Apellido"
        onChange={e => escrib(e)}
        value={formLogin.lastName}
        maxLength="50"
      />
      {formLogin.Eemail.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.Eemail}</p>
      ) : null}
      <input
        type="email"
        name="email"
        placeholder="Email"
        onChange={e => escrib(e)}
        value={formLogin.email}
        maxLength="60"
      />
      {formLogin.Ecompany.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.Ecompany}</p>
      ) : null}
      <input
        type="text"
        name="company"
        placeholder="Empresa"
        onChange={e => escrib(e)}
        value={formLogin.company}
        maxLength="50"
      />
      {formLogin.Ephone.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.Ephone}</p>
      ) : null}
      <input
        type="text"
        name="phone"
        placeholder="Teléfono Celular"
        onChange={e => escrib(e)}
        value={formLogin.phone}
        maxLength="17"
      />
      {formLogin.Eposition.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <p>{formLogin.Eposition}</p>
      ) : null}
      <input
        type="text"
        name="position"
        placeholder="Cargo"
        onChange={e => escrib(e)}
        value={formLogin.position}
        maxLength="40"
      />
      {action === 'edit' ? (
        <div>
          {formLogin.Estatus.length !==
          0 /* || formLogin.url.length !== 0 */ ? (
            <p>{formLogin.Estatus}</p>
          ) : null}
          <input
            type="text"
            name="status"
            placeholder="Estatus"
            onChange={e => escrib(e)}
            value={formLogin.status}
            maxLength="40"
          />
        </div>
      ) : null}
      <button type="button" onClick={() => validate0(formLogin)}>
        {action === 'edit' ? 'EDITAR_action' : 'CREAR'}
      </button>
      {action === 'edit' ? (
        <button type="button" onClick={() => showForm0(idClientDetail, false)}>
          Cancelar
        </button>
      ) : null}

      <button type="button" onClick={() => showState()}>
        STATE
      </button>
      <br />
      {/* clientList // MOSTRAR LISTA DE CLIENTES EN LA MISMA PANTALLA AL PRESIONAR ENVIAR
        ? clientList.map((client, i) => (
            <div key={i}>
              <div>
                <h1>Cliente #{i + 1} - </h1> {client.name} {client.lastName}
              </div>

              <div>Email: {client.email}</div>
              <div>Empresa: {client.company}</div>
              <div>Telefono celular: {client.phone}</div>
              <div>Cargo: {client.position}</div>
              <div>Estatus: {formLogin.status}</div>
            </div>
          ))
        : null */}
    </div>
  );
};

export default ClientForm;
