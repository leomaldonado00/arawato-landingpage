import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Loading from '../../Loading/Loading';
import {
  managerList,
  deleteManager,
  // show10,
  // postUser,
  // upDateUser,
  // upDatePart,
  // deleteUser,
  managers, // var
  isSearchLoading, // var
  // scroll10, // var
  error, // var
  showModal, // var abrir modal
  msgModal, // var mensaje del modal
  hideModalManager,
} from '../../ducks/createManager';
import ModalActions from '../../components/ModalActions';

const ManagerList = ({ history }) => {
  const dispatch = useDispatch();
  const managerListTemp = useSelector(state => managers(state)); // obtiene de los selectors el state especificamente "search.movieResults.Search"
  const isLoading = useSelector(state => isSearchLoading(state));
  // const scroll10var = useSelector(state => scroll10(state));
  const errorTemp = useSelector(state => error(state));
  const showModalTemp = useSelector(state => showModal(state));
  const msgModalTemp = useSelector(state => msgModal(state));

  // const [user, setUser] = useState({ userId: '', title: '', body: '' });
  // const [id, setId] = useState('');

  useEffect(() => {
    console.log('useEffect_tmeplate');
    dispatch(managerList()); // en este caso buscará la lista de Gerentes de Proyectos (Manager)
  }, [dispatch]);

  /* useEffect(() => {
    if (scroll10var) {
      window.scroll(0, 10000000);
    }
  }, [scroll10var]); */

  /* function setUserValid(e) {
    setUser({ ...user, [e.target.name]: e.target.value });
    if (e.target.value === '') {
      setUser({ ...user, [e.target.name]: '' });
    }
  }
  const postUser0 = user => {
    dispatch(postUser({ user }));
    setUser({ userId: '', title: '', body: '' }); // *************************************************
    setId('');
  };
  const show10_0 = event => {
    dispatch(show10());
  };
  const upDatePart0 = (id, user) => {
    console.log('upDatePart0', id, user);
    dispatch(upDatePart({ id, user }));
    setUser({ userId: '', title: '', body: '' }); // *************************************************
    setId('');
  };
  const upDateUser0 = (id, user) => {
    dispatch(upDateUser({ id, user }));
    setUser({ userId: '', title: '', body: '' }); // *************************************************
    setId('');
  };
  const deleteUser0 = id => {
    dispatch(deleteUser({ id }));
  };
  const User0 = id => {
    history.push(`/detailPost/${id}`);
  };
  const profile0 = userId => {
    history.push(`/profilePost/${userId}`);
  };
  console.log(errorTemp); */
  const setShow = () => {
    dispatch(hideModalManager());
  };
  const detailManager0 = id => {
    history.push(`/detail-manager/${id}`);
  };
  /* const editClient0 = id => {
    history.push(`/edit-profile-client/${id}`);
  }; */
  const deleteManager0 = idManagerDele => {
    dispatch(deleteManager({ idManagerDele }));
  };

  const renderList = () => {
    // YOUTUBE, Se queda pegado en Loading cuando no encuentra resultados :s
    if (managerListTemp) {
      return managerListTemp.map((user, i) => (
        <div key={user.id}>
          <h1>
            {user.userId} {user.title}
          </h1>
          <div>{user.body}</div>
          <div>address es un objeto</div>
          <button
            type="button"
            onClick={() => {
              detailManager0(user.id);
            }}
          >
            Detail-Manager
          </button>
          {/* <button
            type="button"
            onClick={() => {
              editClient0(user.id);
            }}
          >
            Edit-Client
          </button> */}
          <button
            type="button"
            onClick={() => {
              deleteManager0(user.id);
            }}
          >
            Delete
          </button>
        </div>
      ));
    }
    if (isLoading) {
      return <Loading />;
    }
    if (errorTemp) {
      return (
        <div>
          <h1>{errorTemp}</h1>
          <ModalActions
            showModalTemp={showModalTemp}
            onHide={() => setShow()}
            msgModalTemp={msgModalTemp}
            actionType="MANAGER_LIST(Lista de Gerente de Proyecto)"
          />
        </div>
      );
    }
    return <h1>No results</h1>;
  };

  return (
    <div>
      {/* isLoading ? <Loading /> : null */}

      {/* managerList
        ? managerList.map((user, i) => (
            <div key={user.id}>
              <h1>
                {user.userId} {user.title}
              </h1>
              <div>{user.body}</div>
              <div>address es un objeto</div>
            </div>
          ))
        : null */}

      {/* errorTemp ? <h1>{errorTemp}</h1> : null */}

      {renderList() /* FORMA DE FUNCION ********************************** */}
    </div>
  );
};

export default ManagerList;
