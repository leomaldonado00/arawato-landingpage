import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Loading from '../../Loading/Loading';
import {
  isSearchLoading, // var
  detailManager, // var
  editFormManager, // var muestra o no el formulario de editar cliente /form o detalle
  // comments,
  detailManagerById,
  showFormManager, // action que cambia la var editFormManager para muestra el formulario
  error, // var
  showModal, // var abrir modal
  msgModal, // var mensaje del modal
  hideModalManager,
} from '../../ducks/createManager';
import ModalActions from '../../components/ModalActions';
import ManagerForm from './ManagerForm';

// import { dispatch } from "../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/rxjs/internal/observable/pairs";

const DetailManager = ({ match, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const detailManagerTemp = useSelector(state => detailManager(state));
  const errorTemp = useSelector(state => error(state));
  const editFormManagerTemp = useSelector(state => editFormManager(state));
  const showModalTemp = useSelector(state => showModal(state));
  const msgModalTemp = useSelector(state => msgModal(state));

  const setShow = () => {
    dispatch(hideModalManager());
  };

  const [edit, setEdit] = useState(false);
  // const commentsTemp = useSelector(state => comments(state));

  useEffect(() => {
    // console.log("useEffect_DetailClient");
    console.log('match_DetailManager', match);
    const idManagerDetail = match.params.id;
    dispatch(detailManagerById({ idManagerDetail }));
  }, [dispatch, match]);

  /* const seeComments0 = id => {
    // dispatch(seeComments({ id }));
    history.push(`/comments?ClientId=${id}`);
  }; */
  const editFunc = siono => {
    if (siono === 'show') {
      console.log('show');
      return true;
    }
    if (editFormManagerTemp) {
      return true;
    }
    return false;
  };
  const showFormManager0 = (idManager, show) => {
    dispatch(showFormManager({ idManager, show }));
  };
  console.log('editFormManagerTemp', editFormManagerTemp);

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {detailManagerTemp ? (
        <div>
          {/* <button
            onClick={() =>
              history.push("/pHold", {
                ancla: ">>CONTENIDO DEL ESTADO DE HISTORY LOCATION<<"
              })
            }
          >
            {" "}
            Phold (enviando STATE de history.location){" "}
        </button> */}
          {/* <button onClick={() => history.goBack()}>BACK </button> */}

          {editFormManagerTemp ? (
            <ManagerForm action="edit" idManagerDetail={detailManagerTemp.id} />
          ) : (
            <div>
              {
                <div>
                  {' '}
                  {detailManagerTemp && detailManagerTemp.userId} .{' '}
                  {detailManagerTemp && detailManagerTemp.id} -{' '}
                  {detailManagerTemp && detailManagerTemp.title} :{' '}
                </div>
              }
              {<div> {detailManagerTemp && detailManagerTemp.body} </div>}
              <button
                type="button"
                onClick={() => showFormManager0(detailManagerTemp.id, true)}
              >
                EDITAR Manager
              </button>
            </div>
          )}

          {/* <button
            onClick={() => {
              seeComments0(detailClientTemp.id);
            }}
          >
            VER COMMENTS
        </button> */}
          {/* commentsTemp.map((comm, i) => (
                <div key={comm.id}>
                <h1>
                    {comm.ClientId} - {comm.name}
                </h1>
                <div>{comm.email}</div>
                <p>{comm.bady}</p>
                </div>
            )) */}
        </div>
      ) : null}

      {errorTemp ? (
        <div>
          <h1>{errorTemp}</h1>
          <ModalActions
            showModalTemp={showModalTemp}
            onHide={() => setShow()}
            msgModalTemp={msgModalTemp}
            actionType="DETAIL_MANAGER_BY_ID(Detalle de Gerente de Proyecto)"
          />
        </div>
      ) : null}
    </div>
  );
};

export default DetailManager;
