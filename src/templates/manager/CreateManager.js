import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ManagerForm from './ManagerForm';
import {
  showModal, // var abrir modal
  msgModal, // var mensaje del modal
  hideModalManager,
} from '../../ducks/createManager';
import ModalActions from '../../components/ModalActions';
// import { useDispatch, useSelector } from "react-redux";
// import { addText , saved, cutText} from '../../ducks/access';

// import { saved } from '../../../ducks/access';

const CreateManager = () => {
  const dispatch = useDispatch();
  const showModalTemp = useSelector(state => showModal(state));
  const msgModalTemp = useSelector(state => msgModal(state));

  const setShow = () => {
    dispatch(hideModalManager());
  };
  return (
    <div>
      <h1>
        COMO administrador QUIERO crear un Gerente de proyecto PARA registrarlo
        en Arawato y poder asociarlo a los Proyectos.{' '}
      </h1>
      <ManagerForm action="create" />
      {/* clientList // MOSTRAR LISTA DE CLIENTES EN LA MISMA PANTALLA AL PRESIONAR ENVIAR
        ? clientList.map((client, i) => (
            <div key={i}>
              <div>
                <h1>Cliente #{i + 1} - </h1> {client.name} {client.lastName}
              </div>

              <div>Email: {client.email}</div>
              <div>Empresa: {client.company}</div>
              <div>Telefono celular: {client.phone}</div>
              <div>Cargo: {client.position}</div>
              <div>Estatus: {client.status}</div>
            </div>
          ))
        : null */}
      <ModalActions
        showModalTemp={showModalTemp}
        onHide={() => setShow()}
        msgModalTemp={msgModalTemp}
        actionType="CREATE_MANAGER"
      />
    </div>
  );
};

export default CreateManager;
