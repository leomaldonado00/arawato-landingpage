import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import Slider from 'react-slick';
import CardServices from '../components/CardServices';
import CardAboutUs from '../components/CardAboutUs';
import CardContact from '../components/CardContact';
// import Button from '../components/Button';
import ButtonW from '../components/ButtonW';
import ColumnWrap from '../components/ColumnWrap';
import Layout from '../components/Layout';
import SocialNetworks from '../components/SocialNetworks';

class Home extends Component {
  componentDidMount() {
    if (
      this.props.history.location.state &&
      this.props.history.location.state.ancla
    ) {
      setTimeout(() => {
        const elmnt = document.getElementById(
          this.props.history.location.state.ancla,
        );
        elmnt.scrollIntoView({ behavior: 'smooth' });
      }, 1000);
    }
  }

  render() {
    const stack = {
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 8, // 8
      slidesToScroll: 1,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 2000,
      cssEase: 'linear',
      responsive: [
        {
          breakpoint: 1200, // 1024
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1,
            dots: false,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
          },
        },
        {
          breakpoint: 620,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
          },
        },
      ],
    };

    const cardServicesSlider = {
      dots: true, // NO ESTAN LOS PUNTOS INDICADORES
      infinite: false,
      arrows: true,
      speed: 1200,
      slidesToShow: 2, // PRESENTA INICIALMENTE LA ULTIMA POR LA MITAD
      slidesToScroll: 2, // SE MUEVEN DE A 4

      responsive: [
        // PARA QUE EL SLIDER SEA RESPONSIVE CON BREAKPOINTS !!!!!!!!!!!!!!!!!!!! OOOJOOO
        /* {
          breakpoint: 1150,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true
          }
         }, */
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true,
            arrows: true,
          },
        },
      ],
    };

    return (
      <div>
        <Helmet>
          <title>Arawato Media</title>
          <meta
            name="description"
            content="Arawato es una empresa para el desarrollo de software, digitalizando los procesos de tu idea de negocio a través de la creación de soluciones tecnológicas que impulsen su crecimiento. Diseñamos y elaboramos sitios web,aplicaciones moviles, desarrollo de plataformas web, mantenimiento y continuación de software."
          />
          <meta
            name="keywords"
            content="arawato,media,desarrollo,diseño,software,página,web,aplicaciones,app,móvil,tecnología,informática,negocio,empresa,plataforma,mantenimiento,digital,Caracas,Charallave,Barquisimeto"
          />
        </Helmet>

        <div id="home-section" />
        <Layout history={this.props.history}>
          <SocialNetworks />
          <Intro>
            <Banner1>
              <TextBanner>
                Te acompañamos en la <span>ideación</span> y{' '}
                <span>desarrollo</span> de tu
                <span> proyecto digital.</span>
              </TextBanner>
            </Banner1>
            <div className="total-center">
              <Description marginTop="50px">
                Te hacemos la vida más fácil, digitalizando los procesos de tu
                idea de negocio a través de la creación de soluciones digitales
                que impulsen su crecimiento.
              </Description>
            </div>
          </Intro>
          <div id="service">
            <Title>Servicios</Title>
            <div className="total-center">
              <ServicesCards>
                <CardServices
                  icon="/assets/images/DESARROLLO.png"
                  title="Desarrollo de App móviles"
                  description="Convertimos tu proyecto digital en una aplicación móvil, con soluciones que facilitan la experiencia de tu usuario desde un smartphone."
                />

                <CardServices
                  icon="/assets/images/DEFINICIÓN.png"
                  title="Diseño y desarrollo de sitios web"
                  description="Hacemos de tu website un espacio dinámico para que se convierta en la mejor carta de presentación online ante tus clientes."
                />

                <CardServices
                  icon="/assets/images/Revisión y desarrollo.png"
                  title="Desarrollo de plataformas web"
                  description="Diseñamos y desarrollamos soluciones según los requerimientos de tu proyecto y las necesidades de  los usuarios, permitiendo la implementación y testeo del producto o servicio."
                />

                <CardServices
                  icon="/assets/images/mantenimiento.png"
                  title="Mantenimiento y continuación de software"
                  description="Llevamos a término proyectos digitales comenzados brindando mejores soluciones para tu cliente."
                />
              </ServicesCards>
            </div>
          </div>

          <MoreUs marginTop="5vh ">
            Si tienes alguna duda, puedes escribirnos directamente.
          </MoreUs>
          <div style={{ marginTop: '20px' }}>
            <ButtonW text="CHATEAR POR WHATSAPP" />
          </div>

          <Title marginTop="13vh" id="aboutUs">
            ¿Quienés somos?
          </Title>

          {/* <div className="total-center">
            <Description>
              Te hacemos la vida más facil transformando tus procesos y negocios
              a través de la creacion de soluciones digitales que acompañen tu
              crecimiento.
            </Description>
              </div> */}

          {/* <MoreUs>¿Quieres saber más sobre Arawato?</MoreUs>       // BOTON DE SOBRE NOSOTROS con texto arriba

          <Button
            text="SOBRE NOSOTROS"
          /> */}

          <div className="total-center cardServicesSlider">
            <WidthDefault>
              <Slider {...cardServicesSlider}>
                <div>
                  <CardContact
                    img="/assets/images/contact4.jpg"
                    name="Gregorio Escalona"
                    job="Director Técnico"
                    description="Arawato, es un proyecto de vida, es donde creamos, nos divertimos, nos formamos y creamos oportunidades para quienes tienen el deseo de superación en el área de tecnología de la información. En Arawato creamos día a día el país que merecemos."
                  />
                </div>
                <div>
                  <CardContact
                    img="/assets/images/contact3.jpg"
                    name="Brehyner Rodríguez"
                    job=" Director Ejecutivo"
                    description="Tenemos clara nuestra responsabilidad de construir proyectos para la industria digital del futuro, y para eso trabajamos diariamente."
                  />
                </div>
                <div>
                  <CardContact
                    img="/assets/images/contact1.jpg"
                    name="Antonio Bello"
                    job="Director de Proyectos"
                    description="Arawato ha representado para mí un reto increíble, no sólo en la integración de nuestros  equipos de trabajo, sino en la implementación de metodologías ágiles y la mejora constante de procesos."
                  />
                </div>
                <div>
                  <CardContact
                    img="/assets/images/contact2.jpg"
                    name="Wilfredo Carrillo"
                    job="Gerente de Proyectos"
                    description="En Arawato he aprendido que los retos son menos fuertes cuando estás acompañado de un equipo de gente talentosa y emprendedora."
                  />
                </div>
              </Slider>
            </WidthDefault>
          </div>

          {/* <div className="total-center">            // SLIDER DE CONTACTS.....
            <ContactsCards>
              <CardContact
                img="/assets/images/contact1.jpg"
                name="Antonio Bello"
                job="Cofundador - Director Técnico "
                description="Arawato ha representado para mi un reto increíble, no sólo en la integración de nuestros equipos de trabajo, sino en implementación de metodologías ágiles  y el mejora constante de los procesos."
              />
              <CardContact
                img="/assets/images/contact2.jpg"
                name="Wilfredo Carrillo"
                job=" Director ejecutivo"
                description="En Arawato he aprendido que los retos son menos fuertes cuando estás acompañado de un equipo de gente talentosa y emprendedora."
              />
              <CardContact
                img="/assets/images/contact3.jpg"
                name="Brehyner Rodríguez"
                job=" Director ejecutivo"
                description="Tenemos claro nuestra responsabilidad de construir proyectos para la industria digital del futuro, y para eso trabajamos diariamente."
              />
              <CardContact
                img="/assets/images/contact4.jpg"
                name="Gregorio Escalona"
                job="Lorem ipsum"
                description="Lorem ipsum dolor sit amet consectetur adipiscing elit euismod condimentum eirmod tempor invidut up labore et dolore magna"
              />
            </ContactsCards>
              </div> */}

          <Title marginTop="13vh" id="job">
            ¿Por qué nosotros?
          </Title>
          <div className="total-center flex-column">
            <ColumnWrap
              col="1fr 1fr 1fr"
              colMd="1fr 1fr"
              colSm="1fr"
              maxWidth="1200px"
              justifyItems="center"
            >
              <CardAboutUs
                icon="/assets/images/Equipos pequeños .png"
                title="Equipos pequeños por proyectos"
                description="Un equipo asignado trabajando activamente a tu proyecto"
              />
              <CardAboutUs
                icon="/assets/images/priorización.png"
                title="Priorización de tareas"
                description="Formas parte del equipo de trabajo garantizando el detalle necesario de cada requerimiento."
              />
              <ColumnPosition>
                <CardAboutUs
                  icon="/assets/images/pruebas.png"
                  title="Desarrollo guiado por pruebas (TDD)"
                  description="Trabajamos con metodologías ágiles garantizando entregas funcionales en cada sprint de trabajo."
                />
              </ColumnPosition>
            </ColumnWrap>
            <ColumnWrap maxWidth="900px" colSm="1fr" justifyItems="center">
              <CardAboutUs
                icon="/assets/images/Standups diarios.png"
                title=" Standups diarios (Reuniones de actualización de estatus)"
                description="Standups diarios entre el equipo de desarrollo para verificar el estatus de cada tarea."
              />
              <CardAboutUs
                icon="/assets/images/Verificaciones.png"
                title="Verificaciones del proyecto semanales, quincenales o mensuales."
                description="Validación continua del proyecto, incluyendo tu criterio como dueño del producto."
              />
            </ColumnWrap>

            <div className="total-center">
              <WidthDefault id="stack">
                <Title marginTop="13vh">Stack de Arawato</Title>
                <Slider {...stack}>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo1.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo2.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/phyton-logo.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo4.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo5.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo6.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo7.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo8.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>

                  <div>
                    <ContainerIconStack>
                      <IconStack
                        src="/assets/images/logo3.png "
                        alt="icon-skack"
                      />
                    </ContainerIconStack>
                  </div>
                </Slider>
              </WidthDefault>
            </div>

            <MoreUs marginTop="10px" color="#00beb6" fontSize="20px">
              Tecnologías de vanguardia
            </MoreUs>
          </div>
        </Layout>
      </div>
    );
  }
}

const TextBanner = styled.p`
  font-family: 'Montserrat';
  font-weight: 300;
  color: #403f3f;
  font-size: 56px;
  width: 100%;
  position: absolute;
  top: 223px;
  left: 85px;
  text-align: center;
  max-width: 770px;
  span {
    font-weight: 700;
  }
  @media (max-width: 1200px) {
    font-size: 45px;
    max-width: 620px;
  }
  @media (max-width: 768px) {
    position: relative;
    left: 20px;
    width: calc(100vw - 10vw);
    margin-right: 5vw;
    margin-left: 5vw;
    top: 100px;
  }
  @media (max-width: 620px) {
    font-size: 30px;
    left: 0px;
  }
`;
const WidthDefault = styled.div`
  width: 100%;
  max-width: 1200px;
  @media (max-width: 1200px) {
    width: 90vw;
  }
`;
const ContainerIconStack = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 100px;
  text-align: center;
  @media (max-width: 620px) {
    width: 100%;
  }
`;
const IconStack = styled.img`
  height: 50px;
  width: auto;
  object-fit: contain;
`;
const Banner1 = styled.div`
  background-image: url('/assets/images/banner-v1.jpg');
  background-size: cover;
  background-position-x: center;
  background-position-y: center;
  width: 100%;
  height: 70vh;
  @media (max-width: 992px) {
    background-position-x: left;
  }
  @media (max-width: 768) {
    background-position: center;
  }
`;

const Intro = styled.div`
  width: 100%;
  height: 100vh;
  /* background: red; */
`;
const ColumnPosition = styled.div`
  @media (max-width: 992px) {
    grid-column: 1 / 4;
  }
  @media (max-width: 768px) {
    grid-column: initial;
  }
`;

const Description = styled.p`
  /* max-width: 931px; */
  font-family: 'Montserrat', sans-serif;
  font-size: 18px;
  font-weight: normal;
  line-height: 1.22;
  text-align: center;
  color: #999999;
  margin-top: ${props => props.marginTop};
  padding-bottom: ${props => props.paddingBottom};
  width: ${props => props.width || '920px'};
  max-width: ${props => props.maxWidth};
  font-size: ${props => props.fontSize || '18px'};
  color: ${props => props.color || '#818181'};
`;

const Title = styled.p`
  font-family: 'Montserrat', sans-serif;
  font-size: 28px;
  font-weight: 300;
  line-height: 1.71;
  text-align: center;
  color: #00beb6;
  margin-top: ${props => props.marginTop};
  padding-bottom: ${props => props.paddingBottom};
`;

const MoreUs = styled.p`
  font-size: ${props => props.fontSize || '15px'}; /*15px;*/
  text-align: center;
  margin: auto;
  margin-top: ${props => props.marginTop || '60px'}; /*60px*/
  color: ${props => props.color || 'silver'}; /*silver;*/
`;

const ServicesCards = styled.div`
  width: 100%;
  max-width: 1200px;
  min-height: 40vh;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  justify-items: center;
  /* background: red; */
  padding-top: 5vh;

  @media (max-width: 992px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 40px;
  }
  @media (max-width: 620px) {
    grid-template-columns: 1fr;
  }
`;

export default Home;

/*
   <Link to='/myprofile'>
        <ImgUsD heightUser={props.heightUser} widthUser={props.widthUser} border={props.border} mr={props.mr}>
             <ImgUserImg src={'/instaPortaf/' + props.imgUser + '.jpg'}  alt="imgUser" height={props.heightUser} width={props.widthUser}/>
            
        </ImgUsD>     
      </Link>
  */
