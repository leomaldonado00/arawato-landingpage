import React, { useEffect } from 'react';
import Layout from '../components/Layout';
import CardMantenance from '../components/CardMantenance';

const ServiceDetails = props => {
  useEffect(() => {
    window.scroll(0, 0);
  });

  return (
    <div>
      <Layout arrow history={props.history}>
        <CardMantenance
          alt="DESARROLLO"
          url="/assets/images/DESARROLLO.png"
          title="MANTENIMIENTO"
          subtitle="y continuacion de servicios"
          descript="Previo a un proceso de levantamiento de requerimientos y comprensión del estatus del proyecto, ayudamos en el mantenimiento y culminación de proyectos en fase intermedia."
          maxWidth="1200px"
          widthImg="122px"
          heightImg="122px"
          mt="100px"
          mr="auto"
          mb="0px"
          ml="auto"
          maxWidthText="1000px"
        />
        <CardMantenance
          alt="Grupo 726"
          url="/assets/images/web-movil.png"
          title=""
          subtitle="Desarrollo web y movil"
          descript="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea."
          maxWidth="915px"
          widthImg="65px"
          heightImg="60px"
          mt="100px"
          mr="auto"
          mb="0px"
          ml="auto"
          maxWidthText="800px"
        />
        <CardMantenance
          alt="Trazado 1479"
          url="/assets/images/api.png"
          title=""
          subtitle="Desarrollo Api"
          descript="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea."
          maxWidth="915px"
          widthImg="65px"
          heightImg="60px"
          mt="50px"
          mr="auto"
          mb="0px"
          ml="auto"
          maxWidthText="800px"
        />

        <CardMantenance
          alt="Grupo 724"
          url="/assets/images/e-commers.png"
          title=""
          subtitle="Desarrollo E-commers"
          descript="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea."
          maxWidth="915px"
          widthImg="65px"
          heightImg="60px"
          mt="50px"
          mr="auto"
          mb="50px"
          ml="auto"
          maxWidthText="800px"
        />
      </Layout>
    </div>
  );
};

export default ServiceDetails;
