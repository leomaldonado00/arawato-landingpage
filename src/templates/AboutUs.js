import React, { useEffect } from 'react';
import styled from 'styled-components';
import CardWhoUs from '../components/CardWhoUs';
import Layout from '../components/Layout';

const AboutUs = props => {
  useEffect(() => {
    window.scroll(0, 0);
  });

  return (
    <Layout history={props.history} arrow>
      <WhoUsDiv>
        <ArawMedia>
          <LogoAraw src="/assets/images/logo-arawato.svg" alt="arawato" />
          <TextMedia>Arawato Media</TextMedia>
        </ArawMedia>
        <WhoUsText>
          <WhoTitle>Quienes Somos</WhoTitle>
          <WhoText>
            Arawato Media C.A. nace de la fusión de la estabilidad de las
            tecnologías corporativas tradicionales con el carácter disruptivo de
            los nuevos modelos de innovación para plantear soluciones y
            servicios orientados a la transformación de la vida de las
            organizaciones en experiencias digitales y el cambio de paradigma
            que trae consigo un nuevo mundo corporativo centrado en el cliente.
          </WhoText>
        </WhoUsText>
      </WhoUsDiv>

      <CardWhoUs
        alt="mision"
        url="/assets/images/mission.png"
        title="Misión"
        descript="Somos una empresa que le hace la vida mas facil a sus clientes digitalizando su procesos para impilsar el crecimiento de su negocio."
      />
      <CardWhoUs
        alt="mision"
        url="/assets/images/mission.png"
        title="Misión"
        descript="Somos una empresa que le hace la vida mas facil a sus clientes digitalizando su procesos para impilsar el crecimiento de su negocio."
      />
      <CardWhoUs
        alt="mision"
        url="/assets/images/mission.png"
        title="Misión"
        descript="Somos una empresa que le hace la vida mas facil a sus clientes digitalizando su procesos para impilsar el crecimiento de su negocio."
      />

      <ValuesTitle>Nuestros Valores</ValuesTitle>

      <Value1>
        <ValueText>
          <TitleValue>Responsabilidad</TitleValue>
          <DescripValue>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat voluptua.{' '}
          </DescripValue>
        </ValueText>
        <ValueImg src="/portaf/code.png" alt="value" />
      </Value1>

      <Value2>
        <ValueImg src="/portaf/code.png" alt="value" />

        <ValueText2>
          <TitleValue>Responsabilidad</TitleValue>
          <DescripValue>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat voluptua.{' '}
          </DescripValue>
        </ValueText2>
      </Value2>

      <Value1>
        <ValueText>
          <TitleValue>Responsabilidad</TitleValue>
          <DescripValue>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat voluptua.{' '}
          </DescripValue>
        </ValueText>

        <ValueImg src="/portaf/code.png" alt="value" />
      </Value1>

      <Value2>
        <ValueImg src="/portaf/code.png" alt="value" />

        <ValueText2>
          <TitleValue>Responsabilidad</TitleValue>
          <DescripValue>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat voluptua.{' '}
          </DescripValue>
        </ValueText2>
      </Value2>
    </Layout>
  );
};

const WhoUsDiv = styled.div`
  width: 85%;
  max-width: 1200px;
  display: flex;
  margin: 30px auto 40px auto;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 1200px) {
    justify-content: space-evenly;
  }
  @media (max-width: 992px) {
    justify-content: space-evenly;
    max-width: 750px;
  }
  @media (max-width: 992px) {
    display: grid;
    justify-items: center;
  }
  @media (max-width: 580px) {
    max-width: 400px;
  }
  @media (max-width: 420px) {
    max-width: 300px;
  }
`;
const ArawMedia = styled.div`
  display: flex;
  max-width: 370px;
  justify-content: space-between;
  align-items: center;
  margin-right: 50px;
  @media (max-width: 992px) {
    display: grid;
    margin-right: 0;
    justify-items: center;
    justify-content: center;
  }
`;
const WhoUsText = styled.div`
  max-width: 750px;

  @media (max-width: 1200px) {
    max-width: 550px;
  }
  @media (max-width: 1200px) {
    margin-top: 15px;
  }
`;
const LogoAraw = styled.img`
  width: 157.6px;
  height: 110px;
  object-fit: contain;
  /*border: solid 1px #00beb6;*/
  background-color: #ffffff;
`;
const TextMedia = styled.p`
  width: 182px;
  /*height: 110px;*/
  font-family: Montserrat;
  font-size: 45px;
  font-weight: 200;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: left;
  color: #00beb6;

  @media (max-width: 992px) {
    text-align: center;
    margin: 15px 0 5px 0;
  }
`;
const WhoTitle = styled.p`
  /*width: 228px;*/
  /*height: 34px;*/
  font-family: Montserrat;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin: 0;

  @media (max-width: 992px) {
    text-align: center;
  }
`;
const WhoText = styled.p`
  /*width: 707px;*/
  /*height: 100px;*/
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;

  @media (max-width: 992px) {
    text-align: center;
  }
`;
const ValuesTitle = styled.p`
  font-family: Montserrat;
  font-size: 48px;
  font-weight: 200;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.21;
  letter-spacing: normal;
  text-align: left;
  color: #00beb6;
  margin: 50px auto 40px auto;
  max-width: 1200px;
  width: 85%;

  @media (max-width: 992px) {
    text-align: center;
  }
`;
const Value1 = styled.div`
  width: 65%;
  display: flex;
  margin: 55px auto 0px auto;
  justify-content: space-between;
  align-items: center;
  max-width: 1100px;

  @media (max-width: 992px) {
    flex-direction: column;
    width: 95%;
  }
`;
const Value2 = styled.div`
  width: 65%;
  display: flex;
  margin: 55px auto 0px auto;
  justify-content: space-between;
  align-items: center;
  max-width: 1100px;

  @media (max-width: 992px) {
    flex-direction: column-reverse;
    width: 95%;
  }
`;
const ValueText = styled.div`
  display: grid;
  margin-right: 50px;
  width: 55%;
  @media (max-width: 992px) {
    margin-right: 0;
    width: 100%;
  }
`;
const ValueText2 = styled.div`
  display: grid;
  margin-left: 50px;
  width: 55%;

  @media (max-width: 992px) {
    margin-left: 0;
    width: 100%;
  }
`;
const TitleValue = styled.p`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;

  @media (max-width: 992px) {
    text-align: center;
  }
`;
const DescripValue = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 2;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;

  @media (max-width: 992px) {
    text-align: center;
  }
`;
const ValueImg = styled.img`
  width: 260px;
  height: 320px;
`;

export default AboutUs;
