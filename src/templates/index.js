import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Home';
import ServiceDetails from './ServiceDetails';
import AboutUs from './AboutUs';
import CreateProfClient from './client/CreateProfClient';
import ClientList from './client/ClientList';
import DetailClient from './client/DetailClient';

import Detail from '../components/Detail';
import CreateManager from './manager/CreateManager';
import ManagerList from './manager/ManagerList';
import DetailManager from './manager/DetailManager';

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={Home} />
      <Route path="/detail" component={Detail} />
      <Route path="/service-details" component={ServiceDetails} />
      <Route path="/about-us" component={AboutUs} />
      <Route path="/create-profile-client" component={CreateProfClient} />
      <Route path="/client-list" component={ClientList} />
      <Route path="/detail-client/:id" component={DetailClient} />
      <Route path="/create-manager" component={CreateManager} />
      <Route path="/manager-list" component={ManagerList} />
      <Route path="/detail-manager/:id" component={DetailManager} />
    </div>
  </Router>
);

export default Routes;
