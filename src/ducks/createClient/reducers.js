import * as types from './types';

const InitialState = {};
let usersVar = []; // para resguardar la varieble de estado state.users
// const savedVar = [];

// let saved = [];

export default function creaClientStore(state = InitialState, action) {
  switch (action.type) {
    case types.INITIAL_USERS_START:
      console.log('reducer_INITIAL_USERS_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        users: null,
        showModal: false,
      };
    case types.INITIAL_USERS_COMPLETE:
      console.log('reducer_INITIAL_USERS_COMPLETE');
      const initial10 = action.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
      usersVar = initial10; // guardo los primeros 10
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [...usersVar],
        showModal: false,
      };
    case types.INITIAL_USERS_ERROR:
      console.log(action);
      // action.error.response.status
      return {
        ...state,
        error: action.error, // .message
        isLoading: false,
        users: null,
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Error al visualizar lista de Clientes: ${action.error}`,
      };
    // DETAIL_CLIENT_BY_ID
    case types.DETAIL_CLIENT_BY_ID_START:
      console.log('reducer_DETAIL_CLIENT_BY_ID_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        detailClient: null,
        showModal: false,
      };
    case types.DETAIL_CLIENT_BY_ID_COMPLETE:
      console.log('reducer_DETAIL_CLIENT_BY_ID_COMPLETE', action.detailClient);
      return {
        ...state,
        isLoading: false,
        error: null,
        detailClient: action.detailClient.data,
        editForm: false, // NO SE MUESTRA EL FORMULARIO DE EDITAR CLIENTE : Si se muestra detalle del cliente
        showModal: false,
      };
    case types.DETAIL_CLIENT_BY_ID_ERROR:
      console.log(action.error);
      // action.error.response.status
      return {
        ...state,
        error: action.error, // .message
        isLoading: false,
        detailClient: null,
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Error al visualizar Detalle de Cliente: ${action.error}`,
      };
    // DELETE_CLIENT
    case types.DELETE_CLIENT_START:
      console.log('reducer_DELETE_CLIENT_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        users: null,
      };
    case types.DELETE_CLIENT_COMPLETE:
      console.log('reducer_DELETE_CLIENT_COMPLETE', action.dataDele);
      const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
      usersVar = user10; // guardo los primeros 10
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [...usersVar],
      };
    case types.DELETE_CLIENT_ERROR:
      console.log(action.error.request);
      // action.error.response.status
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        users: null,
      };
    // EDIT_CLIENT
    case types.EDIT_CLIENT_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA EDITAR CLIENTE PUT O PATCH ...
      console.log('reducer_EDIT_CLIENT_START');
      return {
        ...state,
        /* error: null,
        isLoading: true,
        users: null, */
        editForm: false, // NO SE MUESTRA EL FORMULARIO DE EDITAR CLIENTE : aparece detalle del cliente
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Se ha Editado el cliente ${action.payload.idClientDetail} de forma exitosa`,
      };
    /* case types.EDIT_CLIENT_COMPLETE:
    console.log('reducer_EDIT_CLIENT_COMPLETE', action.dataDele);
    const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
    usersVar = user10; // guardo los primeros 10
    return {
      ...state,
      isLoading: false,
      error: null,
      users: [...usersVar],
    };
  case types.EDIT_CLIENT_ERROR:
    console.log(action.error.request);
    // action.error.response.status
    return {
      ...state,
      error: action.error.message,
      isLoading: false,
      users: null,
    }; */

    // SHOW_FORM
    case types.SHOW_FORM_START:
      console.log('reducer_SHOW_FORM_START');
      return {
        ...state,
        /* error: null,
        isLoading: true,
        users: null, */
        editForm: action.payload.show, // SI SE MUESTRA EL FORMULARIO DE EDITAR CLIENTE : esconde detalle del cliente
      };

    // HIDE_MODAL
    case types.HIDE_MODAL_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA CREATEAR CLIENTE PUT O PATCH ...
      console.log('reducer_HIDE_MODAL_START');
      return {
        ...state,
        /* error: null,
  isLoading: true,
  users: null, */
        showModal: false, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "HIDE_MODAL"(deberia ser en sagas "put")
        msgModal: null,
      };

    // CREATE_CLIENT
    case types.CREATE_CLIENT_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA CREATEAR CLIENTE PUT O PATCH ...
      console.log('reducer_CREATE_CLIENT_START');
      return {
        ...state,
        /* error: null,
      isLoading: true,
      users: null, */
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Se ha Creado el cliente ${action.payload.dataLogin.name} ${action.payload.dataLogin.lastName} de forma exitosa`,
      };

    /* case types.CREATE_CLIENT_COMPLETE:
  console.log('reducer_CREATE_CLIENT_COMPLETE', action.dataDele);
  const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
  usersVar = user10; // guardo los primeros 10
  return {
    ...state,
    isLoading: false,
    error: null,
    users: [...usersVar],
  };
case types.CREATE_CLIENT_ERROR:
  console.log(action.error.request);
  // action.error.response.status
  return {
    ...state,
    error: action.error.message,
    isLoading: false,
    users: null,
  }; */

    /* case types.SEARCH_MOVIE_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        movieResults: null,
      };
    case types.SEARCH_MOVIE_COMPLETE:
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResults: action.results.data,
      };
    case types.SEARCH_MOVIE_ERROR:
      console.log('SEARCH_MOVIE_ERROR', action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResults: null,
      };

      

    case types.SEARCH_MOVIE_BY_ID_START: {
      return { ...state, error: null, isLoading: true, movieResult: null }; // movieResults: null PARA QUE NO SE VEA ENTRE LOADING ??????????
    }
    case types.SEARCH_MOVIE_BY_ID_ERROR: {
      console.log(action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResult: null,
      };
    }
    case types.SEARCH_MOVIE_BY_ID_COMPLETE: {
      // console.log(action);
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResult: action.movie.data,
      };
    }

    case types.ADD_TEXT: {
      // console.log("ADD_TEXT action: ", action);
      // return {...state, saved:[...state.saved, action.payload.searchText]};
      savedVar = [...savedVar, action.payload.searchText];
      return { ...state, saved: [...savedVar] };
    }
    case types.CUT_TEXT: {
      savedVar = state.saved.filter(
        (text, index) => index !== action.payload.index,
      ); // Se guardan las filtradas en "let savedVar"
      return { ...state, saved: [...savedVar] };
    } */

    default:
      return state;
  }
}
