import { put, call, takeLatest } from 'redux-saga/effects';
import { get } from 'lodash';
import * as types from './types';
import { apiPlaceHolder } from '../../api';

export function* initialUsers({ payload }) {
  try {
    const iniUsers = yield call(apiPlaceHolder, `/posts`, null, null, 'GET');
    yield put({ type: types.INITIAL_USERS_COMPLETE, iniUsers });
  } catch (error) {
    if (error.response !== undefined) {
      yield put({ type: types.INITIAL_USERS_ERROR, error: error.message });
    } else {
      const errorConex = 'ERROR DE CONEXIÓN';
      yield put({ type: types.INITIAL_USERS_ERROR, error: errorConex });
    }
  }
}

export function* detailClientById({ payload }) {
  try {
    const detailClient = yield call(
      apiPlaceHolder,
      `/posts/${payload.idClientDetail}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.DETAIL_CLIENT_BY_ID_COMPLETE, detailClient });
  } catch (error) {
    if (error.response !== undefined) {
      yield put({
        type: types.DETAIL_CLIENT_BY_ID_ERROR,
        error: error.message,
      });
    } else {
      const errorConex = 'ERROR DE CONEXIÓN';
      yield put({ type: types.DETAIL_CLIENT_BY_ID_ERROR, error: errorConex });
    }
  }
}

export function* deleteClient({ payload }) {
  try {
    const clientDele = yield call(
      apiPlaceHolder,
      `/posts/${payload.idClientDele}`,
      null,
      null,
      'DELETE',
    );
    const iniUsers = yield call(apiPlaceHolder, `/posts`, null, null, 'GET');
    yield put({
      type: types.DELETE_CLIENT_COMPLETE,
      dataDele: { iniUsers, payload, clientDele },
    });
  } catch (error) {
    yield put({ type: types.DELETE_CLIENT_ERROR, error });
  }
}
/* export function* searchMovie({ payload }) {
  try {
    const results = yield call(
      apiCall,
      `&s=${payload.movieName}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.SEARCH_MOVIE_COMPLETE, results });
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_ERROR, error });
  }
}

 export function* searchMovieById({ payload }) {
  try {
    const movie = yield call(
      apiCall,
      `&i=${payload.movieId}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.SEARCH_MOVIE_BY_ID_COMPLETE, movie }); // lanzar accion para que el reducer la detecte (payolad:results)
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_BY_ID_ERROR, error }); // accion en caso de error (payolad:error)
  }
} */

export function* sagaPlaceH() {
  yield takeLatest(types.INITIAL_USERS_START, initialUsers);
  yield takeLatest(types.DETAIL_CLIENT_BY_ID_START, detailClientById);
  yield takeLatest(types.DELETE_CLIENT_START, deleteClient);
  // yield takeLatest(types.SEARCH_MOVIE_BY_ID_START, searchMovieById);
}

// PARA OBTENER DEL STATE REDUCER LAS VARIABLES DE ESTADO...:
export const users = state => get(state, 'creaClientStore.users');
export const isSearchLoading = state => get(state, 'creaClientStore.isLoading');
/* export const movieResults = state =>
  get(state, 'creaClientStore.movieResults.Search');
export const movieResult = state => get(state, 'creaClientStore.movieResult');
export const saved = state => get(state, 'creaClientStore.saved'); */
export const error = state => get(state, 'creaClientStore.error');

export const detailClient = state => get(state, 'creaClientStore.detailClient');

export const editForm = state => get(state, 'creaClientStore.editForm');
export const showModal = state => get(state, 'creaClientStore.showModal');
export const msgModal = state => get(state, 'creaClientStore.msgModal');
