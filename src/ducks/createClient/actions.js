import * as types from './types';

const initialUsers = payload => {
  console.log('ACTION_initialUsers');

  return {
    type: types.INITIAL_USERS_START,
    payload,
  };
};

const detailClientById = payload => {
  console.log('ACTION_detailClientById');

  return {
    type: types.DETAIL_CLIENT_BY_ID_START,
    payload,
  };
};

const deleteClient = payload => {
  console.log('ACTION_deleteClient');

  return {
    type: types.DELETE_CLIENT_START,
    payload,
  };
};

const editClient = payload => {
  console.log('ACTION_editClient');

  return {
    type: types.EDIT_CLIENT_START,
    payload,
  };
};

const createClient = payload => {
  console.log('ACTION_createClient');

  return {
    type: types.CREATE_CLIENT_START,
    payload,
  };
};

const showForm = payload => {
  console.log('ACTION_showForm');

  return {
    type: types.SHOW_FORM_START,
    payload,
  };
};

const hideModal = payload => {
  console.log('ACTION_hideModal');

  return {
    type: types.HIDE_MODAL_START,
    payload,
  };
};

/* const searchMovie = payload => {
  return {
    type: types.SEARCH_MOVIE_START,
    payload
  };
};

const searchMovieById = payload => {
  return {
    type: types.SEARCH_MOVIE_BY_ID_START,
    payload
  };
};

const addText = payload => {
  return {
    type: types.ADD_TEXT,
    payload
  };
};

const cutText = payload => {
  return {
    type: types.CUT_TEXT,
    payload
  };
}; */

export {
  initialUsers,
  detailClientById,
  deleteClient,
  editClient,
  showForm,
  createClient,
  hideModal,
};
