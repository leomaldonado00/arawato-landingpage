import * as types from './types';

const InitialState = {};
let managersVar = []; // para resguardar la varieble de estado state.users
// const savedVar = [];

// let saved = [];

export default function creaManagerStore(state = InitialState, action) {
  switch (action.type) {
    case types.MANAGER_LIST_START:
      console.log('reducer_MANAGER_LIST_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        managers: null,
        showModal: false,
      };
    case types.MANAGER_LIST_COMPLETE:
      console.log('reducer_MANAGER_LIST_COMPLETE');
      const initial10 = action.managers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
      managersVar = initial10; // guardo los primeros 10
      return {
        ...state,
        isLoading: false,
        error: null,
        managers: [...managersVar],
        showModal: false,
      };
    case types.MANAGER_LIST_ERROR:
      console.log(action);
      // action.error.response.status
      return {
        ...state,
        error: action.error, // .message
        isLoading: false,
        managers: null,
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Error al visualizar lista de Gerente de Proyecto: ${action.error}`,
      };
    // DETAIL_MANAGER_BY_ID
    case types.DETAIL_MANAGER_BY_ID_START:
      console.log('reducer_DETAIL_MANAGER_BY_ID_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        detailManager: null,
        showModal: false,
      };
    case types.DETAIL_MANAGER_BY_ID_COMPLETE:
      console.log(
        'reducer_DETAIL_MANAGER_BY_ID_COMPLETE',
        action.detailManager,
      );
      return {
        ...state,
        isLoading: false,
        error: null,
        detailManager: action.detailManager.data,
        editFormManager: false, // NO SE MUESTRA EL FORMULARIO DE EDITAR ManagerE : Si se muestra detalle del Managere
        showModal: false,
      };
    case types.DETAIL_MANAGER_BY_ID_ERROR:
      console.log('DETAIL_MANAGER_BY_ID_ERROR', action.error.request);
      // action.error.response.status
      return {
        ...state,
        error: action.error, // .message
        isLoading: false,
        detailManager: null,
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_CLIENT"(deberia ser en sagas "put")
        msgModal: `Error al visualizar Detalle de Gerente de Proyecto: ${action.error}`,
      };
    // DELETE_MANAGER
    case types.DELETE_MANAGER_START:
      console.log('reducer_DELETE_MANAGER_START');
      return {
        ...state,
        error: null,
        isLoading: true,
        managers: null,
      };
    case types.DELETE_MANAGER_COMPLETE:
      console.log('reducer_DELETE_MANAGER_COMPLETE', action.dataDele);
      const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
      managersVar = user10; // guardo los primeros 10
      return {
        ...state,
        isLoading: false,
        error: null,
        managers: [...managersVar],
      };
    case types.DELETE_MANAGER_ERROR:
      console.log(action.error.request);
      // action.error.response.status
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        managers: null,
      };
    // EDIT_MANAGER
    case types.EDIT_MANAGER_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA EDITAR MANAGERE PUT O PATCH ...
      console.log('reducer_EDIT_MANAGER_START');
      return {
        ...state,
        /* error: null,
        isLoading: true,
        users: null, */
        editFormManager: false, // NO SE MUESTRA EL FORMULARIO DE EDITAR MANAGERE : esconde el Formulario de Manager
      };
    /* case types.EDIT_MANAGER_COMPLETE:
    console.log('reducer_EDIT_MANAGER_COMPLETE', action.dataDele);
    const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
    managersVar = user10; // guardo los primeros 10
    return {
      ...state,
      isLoading: false,
      error: null,
      users: [...managersVar],
    };
  case types.EDIT_MANAGER_ERROR:
    console.log(action.error.request);
    // action.error.response.status
    return {
      ...state,
      error: action.error.message,
      isLoading: false,
      users: null,
    }; */

    // SHOW_FORM
    case types.SHOW_FORM_MANAGER_START:
      console.log('reducer_SHOW_FORM_MANAGER_START');
      return {
        ...state,
        /* error: null,
        isLoading: true,
        users: null, */
        editFormManager: action.payload.show, // SI SE MUESTRA EL FORMULARIO DE EDITAR CLIENTE : esconde detalle del cliente
      };

    // HIDE_MODAL_MANAGER
    case types.HIDE_MODAL_MANAGER_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA CREATEAR CLIENTE PUT O PATCH ...
      console.log('reducer_HIDE_MODAL_MANAGER_START');
      return {
        ...state,
        /* error: null,
isLoading: true,
users: null, */
        showModal: false, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "HIDE_MODAL_MANAGER"(deberia ser en sagas "put")
        msgModal: null,
      };

    // CREATE_MANAGER
    case types.CREATE_MANAGER_START: // SE SUPONE QUE ESTARIA HACIENDO LA LLMADA PARA CREATEAR MANAGERE PUT O PATCH ...
      console.log('reducer_CREATE_MANAGER_START');
      return {
        ...state,
        /* error: null,
    isLoading: true,
    users: null, */
        showModal: true, // SE MUESTRA UN MODAL CON MENSAJE DE LA ACCION "CREATE_MANAGER"(deberia ser en sagas "put")
        msgModal: `Se ha Creado el MANAGER ${action.payload.dataLogin.name} ${action.payload.dataLogin.lastName} de forma exitosa`,
      };

    /* case types.CREATE_MANAGER_COMPLETE:
console.log('reducer_CREATE_MANAGER_COMPLETE', action.dataDele);
const user10 = action.dataDele.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
usersVar = user10; // guardo los primeros 10
return {
  ...state,
  isLoading: false,
  error: null,
  users: [...usersVar],
};
case types.CREATE_MANAGER_ERROR:
console.log(action.error.request);
// action.error.response.status
return {
  ...state,
  error: action.error.message,
  isLoading: false,
  users: null,
}; */

    /* case types.SEARCH_MOVIE_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        movieResults: null,
      };
    case types.SEARCH_MOVIE_COMPLETE:
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResults: action.results.data,
      };
    case types.SEARCH_MOVIE_ERROR:
      console.log('SEARCH_MOVIE_ERROR', action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResults: null,
      };

      

    case types.SEARCH_MOVIE_BY_ID_START: {
      return { ...state, error: null, isLoading: true, movieResult: null }; // movieResults: null PARA QUE NO SE VEA ENTRE LOADING ??????????
    }
    case types.SEARCH_MOVIE_BY_ID_ERROR: {
      console.log(action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResult: null,
      };
    }
    case types.SEARCH_MOVIE_BY_ID_COMPLETE: {
      // console.log(action);
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResult: action.movie.data,
      };
    }

    case types.ADD_TEXT: {
      // console.log("ADD_TEXT action: ", action);
      // return {...state, saved:[...state.saved, action.payload.searchText]};
      savedVar = [...savedVar, action.payload.searchText];
      return { ...state, saved: [...savedVar] };
    }
    case types.CUT_TEXT: {
      savedVar = state.saved.filter(
        (text, index) => index !== action.payload.index,
      ); // Se guardan las filtradas en "let savedVar"
      return { ...state, saved: [...savedVar] };
    } */

    default:
      return state;
  }
}
