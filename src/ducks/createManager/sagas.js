import { put, call, takeLatest } from 'redux-saga/effects';
import { get } from 'lodash';
import * as types from './types';
import { apiPlaceHolder } from '../../api';

export function* managerList({ payload }) {
  try {
    const managers = yield call(apiPlaceHolder, `/posts`, null, null, 'GET');
    yield put({ type: types.MANAGER_LIST_COMPLETE, managers });
  } catch (error) {
    if (error.response !== undefined) {
      yield put({ type: types.MANAGER_LIST_ERROR, error: error.message });
    } else {
      const errorConex = 'ERROR DE CONEXIÓN';
      yield put({ type: types.MANAGER_LIST_ERROR, error: errorConex });
    }
  }
}

export function* detailManagerById({ payload }) {
  try {
    const detailManager = yield call(
      apiPlaceHolder,
      `/posts/${payload.idManagerDetail}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.DETAIL_MANAGER_BY_ID_COMPLETE, detailManager });
  } catch (error) {
    if (error.response !== undefined) {
      yield put({
        type: types.DETAIL_MANAGER_BY_ID_ERROR,
        error: error.message,
      });
    } else {
      const errorConex = 'ERROR DE CONEXIÓN';
      yield put({ type: types.DETAIL_MANAGER_BY_ID_ERROR, error: errorConex });
    }
  }
}

export function* deleteManager({ payload }) {
  try {
    const managerDele = yield call(
      apiPlaceHolder,
      `/posts/${payload.idManagerDele}`,
      null,
      null,
      'DELETE',
    );
    const iniUsers = yield call(apiPlaceHolder, `/posts`, null, null, 'GET');
    yield put({
      type: types.DELETE_MANAGER_COMPLETE,
      dataDele: { iniUsers, payload, managerDele },
    });
  } catch (error) {
    yield put({ type: types.DELETE_MANAGER_ERROR, error });
  }
}
/* export function* searchMovie({ payload }) {
  try {
    const results = yield call(
      apiCall,
      `&s=${payload.movieName}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.SEARCH_MOVIE_COMPLETE, results });
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_ERROR, error });
  }
}

 export function* searchMovieById({ payload }) {
  try {
    const movie = yield call(
      apiCall,
      `&i=${payload.movieId}`,
      null,
      null,
      'GET',
    );
    yield put({ type: types.SEARCH_MOVIE_BY_ID_COMPLETE, movie }); // lanzar accion para que el reducer la detecte (payolad:results)
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_BY_ID_ERROR, error }); // accion en caso de error (payolad:error)
  }
} */

export function* managersagaPlaceH() {
  yield takeLatest(types.MANAGER_LIST_START, managerList);
  yield takeLatest(types.DETAIL_MANAGER_BY_ID_START, detailManagerById);
  yield takeLatest(types.DELETE_MANAGER_START, deleteManager);
  // yield takeLatest(types.SEARCH_MOVIE_BY_ID_START, searchMovieById);
}

// PARA OBTENER DEL STATE REDUCER LAS VARIABLES DE ESTADO...:
export const managers = state => get(state, 'creaManagerStore.managers');
export const isSearchLoading = state =>
  get(state, 'creaManagerStore.isLoading');
/* export const movieResults = state =>
  get(state, 'creaClientStore.movieResults.Search');
export const movieResult = state => get(state, 'creaClientStore.movieResult');
export const saved = state => get(state, 'creaClientStore.saved'); */
export const error = state => get(state, 'creaManagerStore.error');

export const detailManager = state =>
  get(state, 'creaManagerStore.detailManager');

export const editFormManager = state =>
  get(state, 'creaManagerStore.editFormManager');
export const showModal = state => get(state, 'creaManagerStore.showModal');
export const msgModal = state => get(state, 'creaManagerStore.msgModal');
