import * as types from './types';

const managerList = payload => {
  console.log('ACTION_managerList');

  return {
    type: types.MANAGER_LIST_START,
    payload,
  };
};

const detailManagerById = payload => {
  console.log('ACTION_detailManagerById');

  return {
    type: types.DETAIL_MANAGER_BY_ID_START,
    payload,
  };
};

const deleteManager = payload => {
  console.log('ACTION_deleteManager');

  return {
    type: types.DELETE_MANAGER_START,
    payload,
  };
};

const editManager = payload => {
  console.log('ACTION_editManager');

  return {
    type: types.EDIT_MANAGER_START,
    payload,
  };
};

const createManager = payload => {
  console.log('ACTION_createManager');

  return {
    type: types.CREATE_MANAGER_START,
    payload,
  };
};

const showFormManager = payload => {
  console.log('ACTION_showFormManager');

  return {
    type: types.SHOW_FORM_MANAGER_START,
    payload,
  };
};

const hideModalManager = payload => {
  console.log('ACTION_hideModalManager');

  return {
    type: types.HIDE_MODAL_MANAGER_START,
    payload,
  };
};

/* const searchMovie = payload => {
  return {
    type: types.SEARCH_MOVIE_START,
    payload
  };
};

const searchMovieById = payload => {
  return {
    type: types.SEARCH_MOVIE_BY_ID_START,
    payload
  };
};

const addText = payload => {
  return {
    type: types.ADD_TEXT,
    payload
  };
};

const cutText = payload => {
  return {
    type: types.CUT_TEXT,
    payload
  };
}; */

export {
  managerList,
  detailManagerById,
  deleteManager,
  editManager,
  createManager,
  showFormManager,
  hideModalManager,
};
