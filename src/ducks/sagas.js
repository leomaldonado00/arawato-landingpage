import { all } from 'redux-saga/effects';
import { sagaPlaceH } from './createClient/sagas';
import { managersagaPlaceH } from './createManager/sagas';
// import { search } from "./access/sagas";
// import { sagaPlaceH } from "./rebusPhold/sagas";

export default function* rootSaga() {
  yield all([sagaPlaceH(), managersagaPlaceH()]);
}
