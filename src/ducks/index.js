import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import createSagaMiddleware from 'redux-saga'; // saga
// import thunk from 'redux-thunk';
import i18nStore from './i18n';
import creaClientStore from './createClient';
import creaManagerStore from './createManager';

const reducers = combineReducers({
  i18nStore,
  creaClientStore,
  creaManagerStore,
});

const persistConfig = {
  key: 'Atawato',
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware(); // saga

export default () => {
  const store = createStore(
    persistedReducer,
    undefined,
    compose(applyMiddleware(sagaMiddleware)), // saga
  );
  const persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
};
