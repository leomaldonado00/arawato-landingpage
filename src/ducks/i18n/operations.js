import * as types from './types';
import en from '../../i18n/en';
import es from '../../i18n/es';
import langStrings from '../../i18n';

const setEn = () => dispatch => {
  langStrings.setLanguage('en')
  dispatch({ type: types.SET_EN, lang: en });
};

const setEs = () => dispatch => {
  langStrings.setLanguage('es')
  dispatch({ type: types.SET_ES, lang: es });
};
export { setEn, setEs };
