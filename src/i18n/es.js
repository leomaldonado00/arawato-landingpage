const es = {
  services: 'Servicios',
  stack: 'Stack',
  about_us: '¿Quienés somos?',
  why_we: '¿Por qué nosotros?',
  contact: 'Contacto',
  spanish: 'Español',
  english: 'Ingles',
};

export default es;
