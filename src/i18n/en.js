const en = {
  services: 'Services',
  stack: 'Stack',
  about_us: 'About us',
  why_we: 'Why We?',
  contact: 'Contact',
  spanish: 'Spanish',
  english: 'English',
};

export default en;
