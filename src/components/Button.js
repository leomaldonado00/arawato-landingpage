import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const Button = props => {
  return (
    <ButtonDiv onClick={props.onClick}>
      <ButtonB>{props.text}</ButtonB>
    </ButtonDiv>
  );
};

const ButtonDiv = styled.div`
  height: auto;
  /*margin-top:20px;*/
  width: 330px;
  margin: auto;
  cursor: pointer;
  @media (max-width: 380px) {
    width: 90%;
  }
`;

const ButtonB = styled.button`
  margin: 20px auto 0px auto;
  border-radius: 40px;
  color: #00beb6;
  background: transparent;
  opacity: 1;
  border: 1px solid #00beb6;
  font-size: 15px;
  width: 100%;
  height: 35px;
  outline: none;
  font-weight: 600;
  padding: 5px 9px;
  text-align: center;
  text-transform: inherit;
  text-overflow: ellipsis;
  cursor: pointer;
  transition-duration: 0.4s;
  transition-timing-function: ease-out;
  @media (max-width: 380px) {
    font-size: 13px;
    height: 32px;
  }
  :hover {
    background-color: #00beb6;
    color: #fff;
  }
`;

export default Button;
