import React from 'react';
import styled from 'styled-components';
import {
  FaTwitterSquare,
  FaFacebookSquare,
  FaInstagram,
  FaWhatsapp,
} from 'react-icons/fa';

const SocialNetworks = () => {
  const phones = [
    '584142528976',
    '584143052069',
    '584123962875',
    '584129922941',
  ]; // NUMEROS CEL
  const phoneIndex = Math.floor(Math.random() * 4);

  return (
    <div>
      <Container>
        <WrapIcon
          href="https://twitter.com/arawatomedia?lang=es"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaTwitterSquare />
        </WrapIcon>
        <WrapIcon
          href="https://facebook.com/arawatomedia/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaFacebookSquare />
        </WrapIcon>
        <WrapIcon
          href="https://www.instagram.com/arawatomedia/?hl=es-la"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaInstagram />
        </WrapIcon>
        <WrapIcon
          href={`https://api.whatsapp.com/send?phone=${phones[phoneIndex]}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaWhatsapp />
        </WrapIcon>
      </Container>
    </div>
  );
};
const Container = styled.div`
  width: 50px;
  height: 200px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.016);
  background-color: rgba(255, 255, 255, 0.3);
  position: fixed;
  right: 0px;
  top: calc(50vh - 200px);
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  transition-duration: 0.6s;
  transition-timing-function: ease-out;
  :hover {
    background-color: rgba(255, 255, 255, 1);
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  }
  @media (max-width: 620px) {
    display: none;
  }
  @media (max-height: 600px) {
    top: calc(80vh - 200px);
  }
`;
const WrapIcon = styled.a`
  transition-duration: 0.2s;
  transition-timing-function: ease-out;
  font-size: 25px;
  color: #4a5763;
  opacity: 0.5;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  :hover {
    font-size: 35px;
    color: #00beb6;
    opacity: 1;
  }
`;
export default SocialNetworks;
