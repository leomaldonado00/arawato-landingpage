import React from 'react';
import styled from 'styled-components';
import Header from './Header';
import Footer from './Footer';

const Layout = props => (
  <div>
    <Header history={props.history} goToId={props.goToId} arrow={props.arrow} />
    <Children>{props.children}</Children>
    <Footer goToId={props.goToId} />
  </div>
);

const Children = styled.div`
  width: 100%;
  min-height: calc(100vh - 320px);
  margin-top: 60px;
  margin-bottom: 20vh;
`;
export default Layout;
