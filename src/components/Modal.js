import React from 'react';
import styled from 'styled-components';
import {
  MdClear,
} from 'react-icons/md';

const Modal = props => (
  <div>
    <Wrapper
      id="myModal"
      style={{ ...props.style, display: props.show ? 'flex' : 'none' }}
    >
      <ContentModal style={props.contentStyle} className={props.className}>
        <BoxChildren>{props.children}</BoxChildren>
      </ContentModal>
      <Close onClick={() => props.onHide()}>
        <MdClear color="#FFF" size={25} />
      </Close>
    </Wrapper>
  </div>
);
const Wrapper = styled.div`
  position: fixed;
  z-index: 9999999; /* 999 */
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0, 0, 0);
  background-color: rgba(0, 0, 0, 0.9);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const BoxChildren = styled.div`
  width: 100%;
  /* padding-left: ${props => props.paddingLeftChildren || '10px'};
  padding-right: ${props => props.paddingRightChildren || '10px'};
  padding-top: ${props => props.paddingTopChildren || '30px'} */
  padding-bottom: 10px;
  @media(max-width: 620px){
    width: 100%;
  }
`;
const ContentModal = styled.div`
  width: 95vw;
  max-width: 600px;
  min-height: 550px;
  background-color: #fff;
  @media (max-height: 620px) {
    min-height: 80vh;
  }
`;

const Close = styled.div`
  /* background-color: red; */
  width: 20px;
  height: 20px;
  top: 10px;
  right: 20px;
  position: absolute;
  font-size: 20px;
  cursor: pointer;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  :hover {
    opacity: 0.5;
  }
`;

export default Modal;
