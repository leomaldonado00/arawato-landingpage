import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const CardAboutUs = props => {
  return (
    <CardAboutUsDiv>
      <ImgAboutUs src={props.icon} alt="desarrollo" />
      <Text>
        <Tittle>{props.title}</Tittle>
        <Description>{props.description}</Description>
      </Text>
    </CardAboutUsDiv>
  );
};

const CardAboutUsDiv = styled.div`
  width: 100%;
  max-width: 300px;
  height: auto;
  max-width: 300px;
  display: grid;
  grid-gap: 15px;
  justify-items: center;
  grid-template-rows: min-content min-content min-content;
  margin-top: 100px;
`;
const ImgAboutUs = styled.img`
  width: auto;
  height: 80px;
`;
const Tittle = styled.h3`
  font-size: 20px;
  color: #1a1a1a;
  margin: 0px 0px 15px 0px;
  text-align: center;
`;
const Description = styled.p`
  font-size: 15px;
  color: gray;
  margin: 0;

  border-radius: 3px;
  text-align: center;
`;
/* const DescriptionDiv = styled.div`
    width:100%;
    height:auto;
    max-width: 175px;
    /*max-height:${props => props.hover ? "auto" : "35px"};
    overflow-y:${props => props.hover ? "visible" : "hidden"};
    position: relative; /****************************************************************** 
`; */
/* const DescripDif = styled.div`
    width:100%;
    height:35px;
    position:absolute;
    background:lime;
    background: ${props => props.hover ? "transparent" : "linear-gradient(to bottom, transparent, white)"}; 
`; */
const Text = styled.div`
 width:100%;
 height:auto;
 display: grid;
    justify-content: center;
    padding: 15px 0px;
 /*background:${props => (props.hover ? '#fafafa' : 'transparent')};*/
 /*border-radius:12px;*/

`;

export default CardAboutUs;
