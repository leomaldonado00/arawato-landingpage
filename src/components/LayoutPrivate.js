import React from 'react';
import styled from 'styled-components';
import SideBar from './SideBar';

const LayoutPrivate = props => (
  <WrapLayout>
    <SideBar
      history={props.history}
      goToId={props.goToId}
      arrow={props.arrow}
    />
    <Children bg={props.background}>{props.children}</Children>
  </WrapLayout>
);

const Children = styled.div`
  width: 100%;
  min-height: 100vh; /* calc(100vh - 320px)*/
  /*margin-top: 20px;*/
  /*margin-bottom: 20vh;*/
  background: ${props => props.bg};
`;
const WrapLayout = styled.div`
  width: 100%;
  display: flex;
  min-height: 100vh; /* calc(100vh - 320px)*/
  /*min-height: calc(100vh - 320px);*/
  /*margin-top: 60px;*/
  /*margin-bottom: 20vh;*/
`;
export default LayoutPrivate;
