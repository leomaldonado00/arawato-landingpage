import React from 'react';
import styled from 'styled-components';
/// import { Link } from "react-router-dom";

const ImgUser = props => {
  return (
    <ImgUsD
      heightUser={props.heightUser}
      widthUser={props.widthUser}
      border={props.border}
      mr={props.mr}
    >
      <ImgUserImg
        src={props.imgUser}
        alt="imgUser"
        height={props.heightUser}
        width={props.widthUser}
      />
    </ImgUsD>
  );
};
//const propi = props;

const ImgUsD = styled.div`
  width: ${props => props.widthUser};
  height: ${props => props.heightUser};
  background: transparent; /*red*/
  border-radius: 50%;
  margin-right: ${props => (props.mr === 'no' ? '0' : '13px')};

  border: ${props => (props.border ? '2px purple solid' : '0')};
`;
const ImgUserImg = styled.img`
  border-radius: 50%;
  /* cursor: pointer; */
  object-fit: cover;
`;

export default ImgUser;
