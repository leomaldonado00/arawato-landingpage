import React from 'react';
import styled from 'styled-components';
import ImgUser from './ImgUser';
// import { Link } from "react-router-dom";
import { FaTwitterSquare, FaFacebookSquare, FaInstagram } from 'react-icons/fa';

const ListPrivate = () => {
  return (
    <SectionList>
      <Top>
        <ImgUser
          heightUser="44px"
          widthUser="44px"
          imgUser="/assets/images/client.jpg"
          mr="15px"
        />
        <UserName>Admin Name</UserName>
      </Top>
      <TitleList marginL="auto" marginR="auto">Clientes</TitleList>
      <DescripP marginL="auto" marginR="auto">
        Esta es la lista de clientes, puedes agregar, editar, quitar.
      </DescripP>
      <TableList>
        <TitleList>Nombre</TitleList>
          <TitleList>Correo electrónico</TitleList>
          <TitleList>Empresa</TitleList>
          <TitleList>Estatus</TitleList>
          <TitleList>Acciones</TitleList>
          <DescripP>Gregorio Escalona</DescripP>
          <DescripP>gregorio_escalona@arawato.co</DescripP>
          <DescripP>Arawato</DescripP>
          <DescripP>Activo</DescripP>
          <DescripP>Acciones</DescripP>
      </TableList>

      {/* renderList() /* FORMA DE FUNCION ********************************** */}
    </SectionList>
  );
};

const Trow = styled.div`
width:100%;
/*display: flex;
    justify-content: space-between;*/
    `;
const TableList = styled.div`
max-width:1200px;
  width: 100%;
  display:grid;

  max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
`;
const SectionList = styled.div`
  width: calc(100% - 50px);
  padding: 25px;
`;
const TitleList = styled.p`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;

  max-width: 1200px;
    margin-left: ${props => props.marginL || "0"};
    margin-right: ${props => props.marginR || "0"};
`;
const DescripP = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;

  max-width: 1200px;
    margin-left: ${props => props.marginL || "0"};
    margin-right: ${props => props.marginR || "0"};
`;
const UserName = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
`;
const Top = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export default ListPrivate;
