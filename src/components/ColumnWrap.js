import styled from 'styled-components';

const ColumnWrap = styled.div`
  display: ${props => props.display || 'grid'};
  grid-template-columns: ${props => props.col || '1fr 1fr'};
  grid-template-rows: ${props => props.rows};
  justify-items: ${props => props.justifyItems};
  align-items: ${props => props.alignItems};
  grid-gap: ${props => props.gridGrap || '10px'};
  width: ${props => props.width || '100%'};
  max-width: ${props => props.maxWidth};
  margin-top: ${props => props.marginTop};
  margin: ${props => props.marginx};
  min-width: ${props => props.minWidth};
  margin-bottom: ${props => props.marginBottom};
  background: ${props => props.backgroundColor};
  border-bottom: ${props => props.borderBottom};
  border-top: ${props => props.borderTop};
  border-left: ${props => props.borderLeft};
  min-height: ${props => props.minHeight};
  height: ${props => props.height};
  align-items: ${props => props.alignItems};

  /* ------------Responsive------------ */
  @media (max-width: ${props => props.sizeMd || '992px'}) {
    grid-template-columns: ${props => props.colMd};
    display: ${props => props.displayx};
    margin-top: ${props => props.marginTopp};
  }
  @media (max-width: ${props => props.sizeSm || '768px'}) {
    grid-template-columns: ${props => props.colSm};
    min-height: ${props => props.minHeightx};
  }
  @media (max-width: ${props => props.sizeXs || '620px'}) {
    grid-template-columns: ${props => props.colXs};
  }
  @media (max-width: ${props => props.sizeXs || '480px'}) {
    grid-template-columns: ${props => props.colSl};
  }
`;

export default ColumnWrap;
