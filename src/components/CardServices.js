import React, { useState } from 'react';
import styled from 'styled-components';

const CardServices = props => {
  const [hover, setHover] = useState(false);
  return (
    <CardServicesDiv
      onClick={props.onClick}
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
    >
      <ImgService src={props.icon} alt="desarrollo" />
      <Text hover={hover}>
        <Tittle>{props.title}</Tittle>
        <DescriptionDiv hover={hover}>
          <DescripDif hover={hover} />
          <Description hover={hover}>{props.description}</Description>
        </DescriptionDiv>
      </Text>
    </CardServicesDiv>
  );
};

const CardServicesDiv = styled.div`
  width: 100%;
  height: 350px;
  /* cursor: pointer; */
  max-width: 245px;
  display: grid;
  grid-gap: 15px;
  justify-items: center;
  grid-template-rows: min-content min-content min-content;
`;
const ImgService = styled.img`
  width: auto;
  height: 110px;
`;
const Tittle = styled.p`
  width: 100%;
  max-width: 230px;
  height: 50px;
  font-family: 'Montserrat';
  font-size: 18px;
  font-weight: 600;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;
`;
const Description = styled.p`
  font-family: 'Montserrat';
  font-size: 14px;
  line-height: 1.29;
  text-align: center;
  color: #4a5763;

  margin: 0;
  background: ${props =>
    props.hover
      ? 'transparent'
      : 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,0) 100%)'};

  border-radius: 3px;
  text-align: center;
`;
const DescriptionDiv = styled.div`
  width: 100%;
  height: auto;
  max-width: 220px;
  max-height: ${props => (props.hover ? 'auto' : '35px')};
  overflow-y: ${props => (props.hover ? 'visible' : 'hidden')};
  position: relative; /****************************************************************** */
`;
const DescripDif = styled.div`
  width: 100%;
  height: 35px;
  position: absolute;
  background: lime;
  transition-duration: 0.6s;
  transition-timing-function: ease-out;
  background: ${props =>
    props.hover
      ? 'transparent'
      : 'linear-gradient(to bottom, transparent, white)'};
`;
const Text = styled.div`
  width: 100%;
  height: auto;
  display: grid;
  justify-content: center;
  padding: 15px 0px;
  /* transition-duration: 0.3s;
  transition-timing-function: ease-out; */
  background: ${props => (props.hover ? 'rgb(233, 232, 232)' : 'transparent')};
  border-radius: 12px;
`;

export default CardServices;
