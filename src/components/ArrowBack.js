import React from 'react';
import styled from 'styled-components';
import { MdArrowBack } from 'react-icons/md';

const ArrowBack = props => (
  <Container onClick={() => props.history.goBack()}>
    <MdArrowBack />
  </Container>
);

const Container = styled.div`
  width: 30px;
  height: 30px;
  background-color: #fff;
  color: #ea4335;
  border-radius: 50%;
  font-size: 20px;
  cursor: pointer;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    background-color: #ea4335;
    color: #fff;
  }
`;

export default ArrowBack;
