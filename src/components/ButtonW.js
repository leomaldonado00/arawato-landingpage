import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const ButtonW = ({ text }) => {
  const phones = ["584142528976", "584143052069", "584123962875", "584129922941"];  // NUMEROS CEL
  const phoneIndex =  Math.floor(Math.random() * 4);
  return (
    <a
      href={`https://api.whatsapp.com/send?phone=${phones[phoneIndex]}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <ButtonDiv>
        <WhatsappDiv>
          <ImgWhatsapp src="/assets/images/whatsapp.svg" alt="whatsapp" />
        </WhatsappDiv>
        <ButtonB>{text}</ButtonB>
      </ButtonDiv>
    </a>
  );
};

const ButtonDiv = styled.div`
  /*margin-top:20px;*/
  width: 330px;
  margin: auto;
  display: flex;

  height: 46px;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  margin-top: ${props => props.marginTop};
  padding-bottom: ${props => props.paddingBottom};
  @media (max-width: 380px) {
    width: 90%;
  }
`;

const ButtonB = styled.button`
  border-radius: 40px;
  color: #00beb6;
  background: transparent;
  opacity: 1;
  border: 1px solid #00beb6;
  transition-duration: 0.6s;
  transition-timing-function: ease-out;
  font-size: 15px;
  width: 310px;
  height: 40px; /* outline: none; */
  outline: none;
  font-weight: 600;
  padding: 5px 9px;
  text-align: center;
  text-transform: inherit;
  text-overflow: ellipsis;

  border-left: none;
  margin-left: 17px;
  cursor: pointer;
  :hover {
    background: #00beb6;
    color: #fff;
  }
  @media (max-width: 380px) {
    padding-left: 19px;
    font-size: 13px;
    height: 32px;
  }
`;
const ImgWhatsapp = styled.img`
  width: 100%;
  height: 100%;
`;
const WhatsappDiv = styled.div`
  position: absolute;

  border-radius: 50%;
  border: 4px solid white;
  width: 40px;
  height: 40px;
  background: white;
  border: 3px solid white;

  @media (max-width: 380px) {
    width: 32px;
    height: 32px;
  }
`;

export default ButtonW;
