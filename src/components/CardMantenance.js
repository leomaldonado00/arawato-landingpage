import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const CardMantenance = props => {
  return (
    <CardMantenanceDiv props={props}>
      <ImgCard
        src={props.url}
        alt={props.alt}
        width={props.widthImg}
        height={props.heightImg}
      />
      <CardText props={props}>
        <CardTitle> {props.title} </CardTitle>
        <CardSubtitle> {props.subtitle} </CardSubtitle>
        <CardDescrip>{props.descript}</CardDescrip>
      </CardText>
    </CardMantenanceDiv>
  );
};

const CardMantenanceDiv = styled.div`
  width: 85%;
  max-width: ${props => props.props.maxWidth};
  /* margin: 20px auto 0px auto; */
  margin-top: ${props => props.props.mt};
  margin-right: ${props => props.props.mr};
  margin-bottom: ${props => props.props.mb};
  margin-left: ${props => props.props.ml};
  min-height: 120px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 720px) {
    display: grid;
    justify-items: center;
    grid-gap: 20px;
    width: calc(95% - 40px);
    padding: 0px 20px 20px 20px;
  }
`;
const ImgCard = styled.img`
  object-fit: contain;
  margin: 0px 40px 0px 0;

  @media (max-width: 720px) {
    margin: 15px 0px 0px 0px;
  }
`;
const CardText = styled.div`
  max-width: ${props => props.props.maxWidthText}; /* 900px; */
  min-height: 110px;
  margin: 0px 0px 0px 0px;

  @media (max-width: 720px) {
    margin: 0;
  }
`;
const CardTitle = styled.p`
  font-family: Montserrat;
  font-size: 28px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  text-align: left;
  color: #00beb6;
  margin: 0;

  @media (max-width: 720px) {
    text-align: center;
  }
`;
const CardSubtitle = styled.p`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin: 0;

  @media (max-width: 720px) {
    text-align: center;
  }
`;
const CardDescrip = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin: 15px 0 0 0;

  @media (max-width: 720px) {
    text-align: center;
  }
`;

export default CardMantenance;
