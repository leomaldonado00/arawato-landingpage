import React from 'react';
import styled from 'styled-components'
// import { PreviousMap } from 'postcss';


const CardContact = (props) => {

    return(
        <CardContactDiv >
        <ImgContact src={props.img}  alt="contact"  />
            <Text>
            <Tittle>{props.name}</Tittle>
            <Subtittle>{props.job}</Subtittle>
            <Line> </Line> 
                <DescriptionDiv>
                    <ImgQuotes0 src='/portaf/quote0.png'  alt="quote0"  />
                    <Description >{props.description}</Description> 
                    <ImgQuotes1 src='/portaf/quote1.png'  alt="quote1"  />
                </DescriptionDiv>
            </Text>
        </CardContactDiv>
)
}

  
  const CardContactDiv = styled.div`
    width:100%;
    height:auto;
    /*background:aqua;*/
    max-width: 490px;
    display: grid;
    grid-gap: 15px;
    justify-items: center;
    grid-template-rows: min-content min-content min-content;
    margin-top:40px;
`;
const ImgContact = styled.img`
width: 150px;
    height: 150px;
    border-radius: 50%;
    object-fit: cover;
`;
const Tittle = styled.h3`
  font-family: 'Montserrat';
  font-size: 18px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;

  @media (max-width: 520px) {
  margin:0;  
  }
 `;
 const Subtittle = styled.h6`
   font-family: 'Montserrat';
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;
`;
const Line = styled.div`
width:55%;
margin: 18px 0 8px 0;
background-color: #efefef;
    height: 2px;

    @media (max-width: 520px) {
  margin:0 0 8px 0;  
  }
`;
const Text = styled.div`
 width:100%;
 height:auto;
 display: grid;
 padding: 8px 0px; /*background:${props => props.hover ? "#fafafa" : "transparent"};*/
 /*border-radius:12px;*/
 justify-items: center;
`;
const DescriptionDiv = styled.div`
 width:100%;
 height:auto;
 display: flex;
 @media (min-width: 1201px) {
    width: 100%;
  }
 @media (max-width: 1200px) {
    width: 90%;
  }
  @media (max-width: 992px) {
    width: 100%;
  }

`;
const ImgQuotes0 = styled.img`
width: auto;
height:13px;
align-self:flex-start;
`;
const ImgQuotes1 = styled.img`
width: auto;
height:13px;
align-self:flex-end;
`;
const Description = styled.p`
   font-family: 'Montserrat';
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;
 `;

  export default CardContact;