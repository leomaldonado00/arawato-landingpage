import React from 'react';
import styled from 'styled-components'
// import { Link } from "react-router-dom";


const CardWhoUs = (props) => {

    return(
        <CardWhoUsDiv >
            <ImgCard src={props.url} alt={props.alt}  />
            <CardText>
                <CardTitle> {props.title} </CardTitle>
                <CardDescrip>{props.descript}</CardDescrip>
            </CardText>
        </CardWhoUsDiv>
)
}

const CardWhoUsDiv = styled.div`
    width:85%;
    max-width: 1200px;
    margin: 20px auto 0px auto;
    min-height: 120px;
  opacity: 0.5;
  border-radius: 20px;
  background-color: #00beb6;
  display: flex;
    align-items: center;
    justify-content: space-between;

    @media (max-width: 720px) {
        display: grid;
    justify-items: center;
    grid-gap: 20px;  
    width: calc(95% - 40px);
    padding: 0px 20px 20px 20px;
}

   `;  
   const ImgCard = styled.img`
  width: 62px;
  height: 62px;
  object-fit: contain;
  margin: 0px 50px 0px 70px;

  @media (max-width: 720px) {
    margin: 15px 0px 0px 0px;
 
 }

 `;
 const CardText = styled.div`
 max-width:900px;
    min-height: 110px;
    margin: 0px 70px 0px 0px;

    @media (max-width: 720px) {
        margin: 0;
 
 }
`;  
const CardTitle = styled.p`
font-family: Montserrat;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  color: #ffffff;
  text-align: left;
    margin: 0;

    @media (max-width: 720px) {
    
        text-align: center;
 }

`;  
const CardDescrip = styled.p`
font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 0;

  @media (max-width: 720px) {
    
    text-align: center;
}
`;  
 

  export default CardWhoUs;