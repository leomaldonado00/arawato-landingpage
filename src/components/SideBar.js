import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";
import { FaTwitterSquare, FaFacebookSquare, FaInstagram } from 'react-icons/fa';

const SideBar = () => {
  return (
    <SideBarWrap>
      <LogoWrap>
        <LogoAraw
          src="/assets/images/logoPriv.png"
          alt="arawato"
          className="opacity"
        />
      </LogoWrap>
    </SideBarWrap>
  );
};

const SideBarWrap = styled.div`
  background: #4a5763;
  width: 280px;
  min-height: 100vh;
`;

const LogoAraw = styled.img`
  width: 158px;
  height: auto;
  object-fit: contain;
`;
const LogoWrap = styled.div`
  width: 280px;
  height: auto;
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

export default SideBar;
