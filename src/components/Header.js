import React, { useState } from 'react';
import styled from 'styled-components';
import { MdMenu } from 'react-icons/md';
import { FaWhatsapp } from 'react-icons/fa';
import { connect } from 'react-redux';
import Modal from './Modal';
import ArrowBack from './ArrowBack';
import langStrings from '../i18n';
import { setEs, setEn } from '../ducks/i18n';

const Header = props => {
  function langInit(props) {
    // que idioma viene?
    if (props.langCode === 'en') {
      return false;
    }
    return true;
  }

  const [menu, setMenu] = useState(false);
  const [menuLang, setMenuLang] = useState(false);
  const [lang, setLang] = useState(langInit(props)); // COMO SON SOLO DOS IDIOMAS : TRUE (español) y FALSE (ingles)

  function setMenuLang0() {
    setMenuLang(!menuLang);
    setMenu(false);
  }
  function setMenu0() {
    setMenu(!menu);
    setMenuLang(false);
  }

  const [item, setItem] = useState(' ');
  const [show, setShow] = useState(false);

  function changeRoute(route) {
    if (props.history.location.pathname !== '/') {
      props.history.push('/', {
        ancla: route,
      });
    } else if (props.history.location.pathname === '/') {
      const elmnt = document.getElementById(route);
      if (elmnt !== null) {
        elmnt.scrollIntoView({ behavior: 'smooth' });
      }
    } else {
      props.history.push('/');
    }
  }

  const phones = [
    '584142528976',
    '584143052069',
    '584123962875',
    '584129922941',
  ]; // NUMEROS CEL
  const phoneIndex = Math.floor(Math.random() * 4);

  return (
    <div>
      <HeaderDiv history={props.history}>
        <WrapArrowLogo>
          {props.arrow ? <ArrowBack history={props.history} /> : null}
          <LogoAraw
            src="/assets/images/logo-arawato.svg"
            alt="arawato"
            className="opacity"
            onClick={() => {
              changeRoute('home-section');
            }}
          />
        </WrapArrowLogo>
        <Links>
          <WhoDiv>
            <LinkWho
              onClick={() => {
                setItem('who');
                changeRoute('service');
              }}
              item={item}
            >
              {langStrings.services}
            </LinkWho>
          </WhoDiv>
          <ServicesDiv lang={lang.toString()}>
            <LinkServices
              onClick={() => {
                setItem('services');
                changeRoute('aboutUs');
              }}
              item={item}
            >
              {langStrings.about_us}
            </LinkServices>
          </ServicesDiv>
          <TecnoDiv lang={lang.toString()}>
            <LinkTecno
              onClick={() => {
                setItem('tecno');
                changeRoute('job');
              }}
              item={item}
            >
              {langStrings.why_we}
            </LinkTecno>
          </TecnoDiv>
          <ClientsDiv>
            <LinkClients
              onClick={() => {
                setItem('clients');
                changeRoute('stack');
              }}
              item={item}
            >
              {langStrings.stack}
            </LinkClients>
          </ClientsDiv>

          <ContactDiv onClick={() => setShow('true')}>
            <LinkContact>{langStrings.contact}</LinkContact>
          </ContactDiv>

          <LinkW
            href={`https://api.whatsapp.com/send?phone=${phones[phoneIndex]}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaWhatsapp color="#00beb6" size={20} />
          </LinkW>

          {/* <Lang onClick={() => setMenuLang0()}>          // MENU DE LENGUAJES
            {lang ? (
              <LangTitle>{langStrings.spanish}</LangTitle>
            ) : (
              <LangTitle>{langStrings.english}</LangTitle>
            )}
            <MdArrowDropDown size={20} />
          </Lang>
          <LangMin onClick={() => setMenuLang0()}>
            {lang ? (
              <>
                {' '}
                <LangTitle>ES</LangTitle> <MdArrowDropDown size={20} />{' '}
              </>
            ) : (
              <>
                {' '}
                <LangTitle>IN</LangTitle> <MdArrowDropDown size={20} />{' '}
              </>
            )}
            </LangMin> */}

          <Toggle className="opacity">
            <MdMenu size={25} onClick={() => setMenu0(!menu)} />
          </Toggle>
        </Links>

        {menu ? (
          <Menu>
            <MenuItems>
              <ItemMenu onClick={() => changeRoute('service')}>
                {langStrings.services}
              </ItemMenu>
              <ItemMenu onClick={() => changeRoute('aboutUs')}>
                {langStrings.about_us}
              </ItemMenu>
              <ItemMenu onClick={() => changeRoute('job')}>
                {langStrings.why_we}
              </ItemMenu>
              <ItemMenu onClick={() => changeRoute('stack')}>
                {langStrings.stack}
              </ItemMenu>

              <ItemMenuContact
                backgroundColor="#00beb6"
                color="#FFF"
                onClick={() => setShow('true')}
                className="opacity"
              >
                {langStrings.contact}
              </ItemMenuContact>
            </MenuItems>
          </Menu>
        ) : null}

        {menuLang ? (
          <MenuLang>
            <MenuItems>
              <ItemMenu
                onClick={() => {
                  props.setEs();
                  setMenuLang0();
                  setLang(true);
                }}
              >
                {langStrings.spanish}
              </ItemMenu>
              <ItemMenu
                onClick={() => {
                  props.setEn();
                  setMenuLang0();
                  setLang(false);
                }}
              >
                {langStrings.english}
              </ItemMenu>
            </MenuItems>
          </MenuLang>
        ) : null}
      </HeaderDiv>
      <Modal show={show} onHide={() => setShow(!show)}>
        <Background>
          <div
            style={{
              backgroundColor: '#ffffff99',
              height: '100%',
              paddingTop: '30px',
            }}
          >
            <Title>Contacto</Title>
            <Description>
              Para más información contacta con nosotros
            </Description>
          </div>
          <div className="total-center flex-column">
            <div>
              <div className="total-center">
                <a
                  href={`https://api.whatsapp.com/send?phone=${phones[phoneIndex]}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="/assets/images/whatsapp.png"
                    alt="icon-whatsapp"
                    height="60px"
                  />
                </a>
              </div>
              <Description fontSize="17px">
                Haz click en el enlace para escribirnos directo a whatsapp
              </Description>

              {/* <Description>
                  https://api.whatsapp.com/send?phone=584142528976
                </Description> */}
            </div>
            <div>
              <div className="total-center">
                <img
                  src="/assets/images/email.png"
                  alt="icon-whatsapp"
                  height="60px"
                />
              </div>
              <DescriptionContact>Contacto@arawato.co</DescriptionContact>
            </div>
          </div>
        </Background>
      </Modal>
    </div>
  );
};

const WrapArrowLogo = styled.div`
  width: 100%;
  max-width: 150px;
  margin-left: 80px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 620px) {
    margin-left: 5px;
  }
`;
const Background = styled.div`
  background-image: url('/assets/images/bg-contact.jpg');
  width: 100%;
  height: 200px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`;
const Title = styled.p`
  font-family: 'Montserrat';
  font-size: ${props => props.fontSize || '28px'};
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: normal;
  text-align: center;
  color: #2b353e;
  margin: 0px;
`;
const Description = styled.p`
  font-family: 'Montserrat';
  font-size: ${props => props.fontSize || '18px'};
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;
`;
const DescriptionContact = styled.p`
  font-family: 'Montserrat';
  font-size: ${props => props.fontSize || '18px'};
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: center;
  color: #4a5763;
  @media (max-width: 520px) {
    margin: 0;
  }
`;
const HeaderDiv = styled.div`
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  height: 70px;
  background: #fff;
  display: flex;
  justify-content: space-between;
  height: 64px;
  align-items: center;
`;
const LogoAraw = styled.img`
  width: auto;
  height: 50px;

  @media (max-width: 620px) {
    margin-left: 5px;
  }
`;
const Links = styled.div`
  height: auto;
  /*background: red;*/
  width: 100%;
  max-width: 705px;
  margin-right: 80px;
  display: flex;
  height: max-content;
  justify-content: space-evenly;
  align-items: center;

  @media (max-width: 992px) {
    justify-content: flex-end;
  }
  @media (max-width: 620px) {
    margin-right: 5px;
  }
`;
const ContactDiv = styled.div`
  width: 84px;
  height: auto;
  width: 100px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50px;
  background-color: #00beb6;
  cursor: pointer;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  border: solid 1px transparent;
  color: #ffffff;
  font-weight: 500;
  :hover {
    color: #00beb6;
    border: solid 1px #00beb6;
    background-color: transparent;
  }
  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkContact = styled.p`
  font-family: 'Montserrat';
  font-size: 14px;
  margin: 0px;
  letter-spacing: normal;
  text-align: left;

  transition-duration: 0.3s;
  transition-timing-function: ease-out;

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkWho = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;

  color: ${props => (props.item === 'who' ? '#00beb6' : 'gray')};
  font-weight: ${props => (props.item === 'who' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #00beb6;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkClients = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'clients' ? '#00beb6' : 'gray')};
  font-weight: ${props => (props.item === 'clients' ? '600' : '300')};
  transition-duration: 0.6s;
  transition-timing-function: ease-out;
  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #00beb6;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkTecno = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'tecno' ? '#00beb6' : 'gray')};
  font-weight: ${props => (props.item === 'tecno' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #00beb6;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkServices = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'services' ? '#00beb6' : 'gray')};
  font-weight: ${props => (props.item === 'services' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #00beb6;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;

const Toggle = styled.div`
  width: auto;
  height: 40px;
  margin-left: 20px;
  display: none;

  @media (max-width: 992px) {
    display: flex;
    align-items: center;
  }
`;
const Menu = styled.div`
  position: absolute;
  background: white;
  top: 55px;
  right: 80px;
  width: 200px;
  height: 230px;
  border-radius: 10px;
  -webkit-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);

  @media (max-width: 620px) {
    right: 10px;
  }
  @media (min-width: 992px) {
    display: none;
  }
`;

const MenuLang = styled.div`
  position: absolute;
  background: white;
  top: 55px;
  right: 80px;
  width: 200px;
  height: 230px;
  border-radius: 10px;
  -webkit-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);

  height: 92px;
  right: 100px;
  @media (max-width: 992px) {
    right: 130px;
  }
  @media (max-width: 480px) {
    right: 45px;
  }
  @media (max-width: 620px) {
    right: 60px;
  }
`;

const MenuItems = styled.div`
  display: grid;
  width: 100%;
  height: 35px;
`;
const ItemMenu = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.86;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin: 0;
  text-align: center;

  padding: 3px 8px;
  cursor: pointer;
  &:hover {
    background-color: gray;
    color: white;
  }
`;
const ItemMenuContact = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.86;
  letter-spacing: normal;
  text-align: left;
  color: ${props => props.color || '#4a5763'};
  margin: 0;
  text-align: center;

  padding: 3px 8px;
  cursor: pointer;
  background-color: ${props => props.backgroundColor};
  /* &:hover {
    background-color: gray;
    color: white;
  } */
`;
const WhoDiv = styled.div`
  width: 80px;
  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const ClientsDiv = styled.div`
  width: 50px;
  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const ServicesDiv = styled.div`
  /* width: 134px; */
  width: ${props => (props.lang === 'true' ? '144px' : '85px' || '85px')};

  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const TecnoDiv = styled.div`
  /* width: 154px; */
  width: ${props => (props.lang === 'true' ? '160px' : '85px' || '85px')};

  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;

const LinkW = styled.a`
  height: min-content;
  margin-right: 10px;
  display: none;

  @media (max-width: 620px) {
    display: flex;
  }
`;

function mapStateToProps(state) {
  return {
    langCode: state.i18nStore.langCode,
  };
}
export default connect(
  mapStateToProps,
  {
    setEs,
    setEn,
  },
)(Header);
