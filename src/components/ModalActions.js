import React from 'react';
import styled from 'styled-components';
import Modal from './Modal';

const ModalActions = props => (
  <Modal show={props.showModalTemp} onHide={props.onHide}>
    <Background>
      <div
        style={{
          backgroundColor: '#ffffff99',
          height: '100%',
          paddingTop: '30px',
        }}
      >
        <h1>MODAL DE {props.actionType}</h1>
      </div>
      <div className="total-center flex-column">
        <div>
          {props.msgModalTemp}
          {/* <Description>
                  https://api.whatsapp.com/send?phone=584142528976
                </Description> */}
        </div>
        <button type="button" onClick={props.onHide}>
          ACEPTAR
        </button>
      </div>
    </Background>
  </Modal>
);
const Background = styled.div`
  background-image: url('/assets/images/bg-contact.jpg');
  width: 100%;
  height: 200px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`;

export default ModalActions;
