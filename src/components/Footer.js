import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";
import { FaTwitterSquare, FaFacebookSquare, FaInstagram } from 'react-icons/fa';

const Footer = () => {
  return (
    <FooterDiv>
      <FooterTop>
        <TopLeft>
          <Location>Caracas - Charallave - Barquisimeto</Location>
          <Mail>arawato@arawato.com.ve</Mail>
        </TopLeft>
        <Social>
          <a href="https://twitter.com/arawatomedia?lang=es" target="blank">
            {' '}
            <FaTwitterSquare size={30} color="#FFF" />{' '}
          </a>
          <a href="https://www.facebook.com/arawatomedia/" target="blank">
            {' '}
            <FaFacebookSquare size={30} color="#FFF" />{' '}
          </a>
          <a
            href="https://www.instagram.com/arawatomedia/?hl=es-la"
            target="blank"
          >
            {' '}
            <FaInstagram size={30} color="#FFF" />{' '}
          </a>
        </Social>
      </FooterTop>
      <FooterBottom>
        <LogoAraw
          src="/assets/images/icon-sample.png"
          alt="arawato"
          className="opacity"
        />
        <CopiRight>
          © 2019 Arawato Media - Todos los derechos reservados
        </CopiRight>
      </FooterBottom>
    </FooterDiv>
  );
};

const FooterDiv = styled.div`
  width: 100%;
  height: auto;
  /* margin-top: 100px; */
`;
const FooterTop = styled.div`
  width: 100%;
  height: 150px;
  background: rgb(0, 82, 82);
  background: linear-gradient(
    90deg,
    rgba(0, 82, 82, 1) 14%,
    rgba(0, 190, 182, 1) 82%
  );
  display: flex;
  justify-content: space-between;
  align-items: center;
  /* padding: 20px 0px; */

  @media (max-width: 630px) {
    display: grid;
    grid-gap: 20px;
    justify-content: center;
    justify-items: center;

    min-height: 170px; /*150px*/
    padding: 10px 0px 10px 0px;
  }
  @media (max-width: 480px) {
    grid-gap: 0px;
    /*min-height: 150px;
    padding: 10px 0px 10px 0px;*/
  }
`;
const FooterBottom = styled.div`
  width: 100%;
  min-height: 50px;
  background: #151a1e;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 0px;

  @media (max-width: 630px) {
    display: grid;
    justify-content: center;
    justify-items: center;
    grid-gap: 10px;

    min-height: 73px;
    padding: 20px 0px;
  }
  /*@media (max-width: 480px) {
    grid-gap: 10px;  }*/
`;
const TopLeft = styled.div`
  width: auto;
  height: auto;
  display: grid;
  margin-left: 80px;
  grid-gap: 10px;

  @media (max-width: 630px) {
    margin-left: 0;

    padding: 10px 10px 10px 10px;
  }
  @media (max-width: 480px) {
    /*padding: 10px 10px 10px 10px;*/
  }
`;
const Social = styled.div`
  width: 120px;
  height: auto;
  margin-right: 80px;
  display: flex;
  justify-content: space-between;
  margin-left: 15px;

  @media (max-width: 630px) {
    margin-right: 0;

    margin-left: 0;
    margin-bottom: 10px;
  }
  @media (max-width: 480px) {
    /*margin-left: 0;
    margin-bottom: 10px;*/
  }
`;
const Location = styled.p`
  font-family: Montserrat;
  font-size: 28px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 0;
  @media (max-width: 630px) {
    text-align: center;
  }
  @media (max-width: 480px) {
    font-size: 24px;
  }
`;
const Mail = styled.p`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 0;
  @media (max-width: 630px) {
    text-align: center;
  }
  @media (max-width: 480px) {
    font-size: 15px;
  }
`;
const CopiRight = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin-right: 80px;

  @media (max-width: 630px) {
    margin-right: 0;
    text-align: center;
    margin-bottom: 0;
  }
  @media (max-width: 480px) {
    margin-bottom: 0;
  }
`;
const LogoAraw = styled.img`
  width: 40px;
  height: auto;
  object-fit: contain;
  margin-left: 80px;

  @media (max-width: 630px) {
    margin-left: 0;
  }
`;

export default Footer;
